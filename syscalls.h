
static const char *syscall_names[] = {
	[__NR_SYSCALL_BASE +   0] = "restart_syscall",
	[__NR_SYSCALL_BASE +   1] = "exit",
	[__NR_SYSCALL_BASE +   2] = "fork",
	[__NR_SYSCALL_BASE +   3] = "read",
	[__NR_SYSCALL_BASE +   4] = "write",
	[__NR_SYSCALL_BASE +   5] = "open",
	[__NR_SYSCALL_BASE +   6] = "close",
	[__NR_SYSCALL_BASE +   8] = "creat",
	[__NR_SYSCALL_BASE +   9] = "link",
	[__NR_SYSCALL_BASE +  10] = "unlink",
	[__NR_SYSCALL_BASE +  11] = "execve",
	[__NR_SYSCALL_BASE +  12] = "chdir",
	[__NR_SYSCALL_BASE +  14] = "mknod",
	[__NR_SYSCALL_BASE +  15] = "chmod",
	[__NR_SYSCALL_BASE +  16] = "lchown",
	[__NR_SYSCALL_BASE +  19] = "lseek",
	[__NR_SYSCALL_BASE +  20] = "getpid",
	[__NR_SYSCALL_BASE +  21] = "mount",
	[__NR_SYSCALL_BASE +  23] = "setuid",
	[__NR_SYSCALL_BASE +  24] = "getuid",
	[__NR_SYSCALL_BASE +  26] = "ptrace",
	[__NR_SYSCALL_BASE +  29] = "pause",
	[__NR_SYSCALL_BASE +  33] = "access",
	[__NR_SYSCALL_BASE +  34] = "nice",
	[__NR_SYSCALL_BASE +  36] = "sync",
	[__NR_SYSCALL_BASE +  37] = "kill",
	[__NR_SYSCALL_BASE +  38] = "rename",
	[__NR_SYSCALL_BASE +  39] = "mkdir",
	[__NR_SYSCALL_BASE +  40] = "rmdir",
	[__NR_SYSCALL_BASE +  41] = "dup",
	[__NR_SYSCALL_BASE +  42] = "pipe",
	[__NR_SYSCALL_BASE +  43] = "times",
	[__NR_SYSCALL_BASE +  45] = "brk",
	[__NR_SYSCALL_BASE +  46] = "setgid",
	[__NR_SYSCALL_BASE +  47] = "getgid",
	[__NR_SYSCALL_BASE +  49] = "geteuid",
	[__NR_SYSCALL_BASE +  50] = "getegid",
	[__NR_SYSCALL_BASE +  51] = "acct",
	[__NR_SYSCALL_BASE +  52] = "umount2",
	[__NR_SYSCALL_BASE +  54] = "ioctl",
	[__NR_SYSCALL_BASE +  55] = "fcntl",
	[__NR_SYSCALL_BASE +  57] = "setpgid",
	[__NR_SYSCALL_BASE +  60] = "umask",
	[__NR_SYSCALL_BASE +  61] = "chroot",
	[__NR_SYSCALL_BASE +  62] = "ustat",
	[__NR_SYSCALL_BASE +  63] = "dup2",
	[__NR_SYSCALL_BASE +  64] = "getppid",
	[__NR_SYSCALL_BASE +  65] = "getpgrp",
	[__NR_SYSCALL_BASE +  66] = "setsid",
	[__NR_SYSCALL_BASE +  67] = "sigaction",
	[__NR_SYSCALL_BASE +  70] = "setreuid",
	[__NR_SYSCALL_BASE +  71] = "setregid",
	[__NR_SYSCALL_BASE +  72] = "sigsuspend",
	[__NR_SYSCALL_BASE +  73] = "sigpending",
	[__NR_SYSCALL_BASE +  74] = "sethostname",
	[__NR_SYSCALL_BASE +  75] = "setrlimit",
	[__NR_SYSCALL_BASE +  77] = "getrusage",
	[__NR_SYSCALL_BASE +  78] = "gettimeofday",
	[__NR_SYSCALL_BASE +  79] = "settimeofday",
	[__NR_SYSCALL_BASE +  80] = "getgroups",
	[__NR_SYSCALL_BASE +  81] = "setgroups",
	[__NR_SYSCALL_BASE +  83] = "symlink",
	[__NR_SYSCALL_BASE +  85] = "readlink",
	[__NR_SYSCALL_BASE +  86] = "uselib",
	[__NR_SYSCALL_BASE +  87] = "swapon",
	[__NR_SYSCALL_BASE +  88] = "reboot",
	[__NR_SYSCALL_BASE +  91] = "munmap",
	[__NR_SYSCALL_BASE +  92] = "truncate",
	[__NR_SYSCALL_BASE +  93] = "ftruncate",
	[__NR_SYSCALL_BASE +  94] = "fchmod",
	[__NR_SYSCALL_BASE +  95] = "fchown",
	[__NR_SYSCALL_BASE +  96] = "getpriority",
	[__NR_SYSCALL_BASE +  97] = "setpriority",
	[__NR_SYSCALL_BASE +  99] = "statfs",
	[__NR_SYSCALL_BASE + 100] = "fstatfs",
	[__NR_SYSCALL_BASE + 103] = "syslog",
	[__NR_SYSCALL_BASE + 104] = "setitimer",
	[__NR_SYSCALL_BASE + 105] = "getitimer",
	[__NR_SYSCALL_BASE + 106] = "stat",
	[__NR_SYSCALL_BASE + 107] = "lstat",
	[__NR_SYSCALL_BASE + 108] = "fstat",
	[__NR_SYSCALL_BASE + 111] = "vhangup",
	[__NR_SYSCALL_BASE + 114] = "wait4",
	[__NR_SYSCALL_BASE + 115] = "swapoff",
	[__NR_SYSCALL_BASE + 116] = "sysinfo",
	[__NR_SYSCALL_BASE + 118] = "fsync",
	[__NR_SYSCALL_BASE + 119] = "sigreturn",
	[__NR_SYSCALL_BASE + 120] = "clone",
	[__NR_SYSCALL_BASE + 121] = "setdomainname",
	[__NR_SYSCALL_BASE + 122] = "uname",
	[__NR_SYSCALL_BASE + 124] = "adjtimex",
	[__NR_SYSCALL_BASE + 125] = "mprotect",
	[__NR_SYSCALL_BASE + 126] = "sigprocmask",
	[__NR_SYSCALL_BASE + 128] = "init_module",
	[__NR_SYSCALL_BASE + 129] = "delete_module",
	[__NR_SYSCALL_BASE + 131] = "quotactl",
	[__NR_SYSCALL_BASE + 132] = "getpgid",
	[__NR_SYSCALL_BASE + 133] = "fchdir",
	[__NR_SYSCALL_BASE + 134] = "bdflush",
	[__NR_SYSCALL_BASE + 135] = "sysfs",
	[__NR_SYSCALL_BASE + 136] = "personality",
	[__NR_SYSCALL_BASE + 138] = "setfsuid",
	[__NR_SYSCALL_BASE + 139] = "setfsgid",
	[__NR_SYSCALL_BASE + 140] = "llseek",
	[__NR_SYSCALL_BASE + 141] = "getdents",
	[__NR_SYSCALL_BASE + 142] = "newselect",
	[__NR_SYSCALL_BASE + 143] = "flock",
	[__NR_SYSCALL_BASE + 144] = "msync",
	[__NR_SYSCALL_BASE + 145] = "readv",
	[__NR_SYSCALL_BASE + 146] = "writev",
	[__NR_SYSCALL_BASE + 147] = "getsid",
	[__NR_SYSCALL_BASE + 148] = "fdatasync",
	[__NR_SYSCALL_BASE + 149] = "sysctl",
	[__NR_SYSCALL_BASE + 150] = "mlock",
	[__NR_SYSCALL_BASE + 151] = "munlock",
	[__NR_SYSCALL_BASE + 152] = "mlockall",
	[__NR_SYSCALL_BASE + 153] = "munlockall",
	[__NR_SYSCALL_BASE + 154] = "sched_setparam",
	[__NR_SYSCALL_BASE + 155] = "sched_getparam",
	[__NR_SYSCALL_BASE + 156] = "sched_setscheduler",
	[__NR_SYSCALL_BASE + 157] = "sched_getscheduler",
	[__NR_SYSCALL_BASE + 158] = "sched_yield",
	[__NR_SYSCALL_BASE + 159] = "sched_get_priority_max",
	[__NR_SYSCALL_BASE + 160] = "sched_get_priority_min",
	[__NR_SYSCALL_BASE + 161] = "sched_rr_get_interval",
	[__NR_SYSCALL_BASE + 162] = "nanosleep",
	[__NR_SYSCALL_BASE + 163] = "mremap",
	[__NR_SYSCALL_BASE + 164] = "setresuid",
	[__NR_SYSCALL_BASE + 165] = "getresuid",
	[__NR_SYSCALL_BASE + 168] = "poll",
	[__NR_SYSCALL_BASE + 169] = "nfsservctl",
	[__NR_SYSCALL_BASE + 170] = "setresgid",
	[__NR_SYSCALL_BASE + 171] = "getresgid",
	[__NR_SYSCALL_BASE + 172] = "prctl",
	[__NR_SYSCALL_BASE + 173] = "rt_sigreturn",
	[__NR_SYSCALL_BASE + 174] = "rt_sigaction",
	[__NR_SYSCALL_BASE + 175] = "rt_sigprocmask",
	[__NR_SYSCALL_BASE + 176] = "rt_sigpending",
	[__NR_SYSCALL_BASE + 177] = "rt_sigtimedwait",
	[__NR_SYSCALL_BASE + 178] = "rt_sigqueueinfo",
	[__NR_SYSCALL_BASE + 179] = "rt_sigsuspend",
	[__NR_SYSCALL_BASE + 180] = "pread64",
	[__NR_SYSCALL_BASE + 181] = "pwrite64",
	[__NR_SYSCALL_BASE + 182] = "chown",
	[__NR_SYSCALL_BASE + 183] = "getcwd",
	[__NR_SYSCALL_BASE + 184] = "capget",
	[__NR_SYSCALL_BASE + 185] = "capset",
	[__NR_SYSCALL_BASE + 186] = "sigaltstack",
	[__NR_SYSCALL_BASE + 187] = "sendfile",
	[__NR_SYSCALL_BASE + 190] = "vfork",
	[__NR_SYSCALL_BASE + 191] = "ugetrlimit",
	[__NR_SYSCALL_BASE + 192] = "mmap2",
	[__NR_SYSCALL_BASE + 193] = "truncate64",
	[__NR_SYSCALL_BASE + 194] = "ftruncate64",
	[__NR_SYSCALL_BASE + 195] = "stat64",
	[__NR_SYSCALL_BASE + 196] = "lstat64",
	[__NR_SYSCALL_BASE + 197] = "fstat64",
	[__NR_SYSCALL_BASE + 198] = "lchown32",
	[__NR_SYSCALL_BASE + 199] = "getuid32",
	[__NR_SYSCALL_BASE + 200] = "getgid32",
	[__NR_SYSCALL_BASE + 201] = "geteuid32",
	[__NR_SYSCALL_BASE + 202] = "getegid32",
	[__NR_SYSCALL_BASE + 203] = "setreuid32",
	[__NR_SYSCALL_BASE + 204] = "setregid32",
	[__NR_SYSCALL_BASE + 205] = "getgroups32",
	[__NR_SYSCALL_BASE + 206] = "setgroups32",
	[__NR_SYSCALL_BASE + 207] = "fchown32",
	[__NR_SYSCALL_BASE + 208] = "setresuid32",
	[__NR_SYSCALL_BASE + 209] = "getresuid32",
	[__NR_SYSCALL_BASE + 210] = "setresgid32",
	[__NR_SYSCALL_BASE + 211] = "getresgid32",
	[__NR_SYSCALL_BASE + 212] = "chown32",
	[__NR_SYSCALL_BASE + 213] = "setuid32",
	[__NR_SYSCALL_BASE + 214] = "setgid32",
	[__NR_SYSCALL_BASE + 215] = "setfsuid32",
	[__NR_SYSCALL_BASE + 216] = "setfsgid32",
	[__NR_SYSCALL_BASE + 217] = "getdents64",
	[__NR_SYSCALL_BASE + 218] = "pivot_root",
	[__NR_SYSCALL_BASE + 219] = "mincore",
	[__NR_SYSCALL_BASE + 220] = "madvise",
	[__NR_SYSCALL_BASE + 221] = "fcntl64",
	[__NR_SYSCALL_BASE + 224] = "gettid",
	[__NR_SYSCALL_BASE + 225] = "readahead",
	[__NR_SYSCALL_BASE + 226] = "setxattr",
	[__NR_SYSCALL_BASE + 227] = "lsetxattr",
	[__NR_SYSCALL_BASE + 228] = "fsetxattr",
	[__NR_SYSCALL_BASE + 229] = "getxattr",
	[__NR_SYSCALL_BASE + 230] = "lgetxattr",
	[__NR_SYSCALL_BASE + 231] = "fgetxattr",
	[__NR_SYSCALL_BASE + 232] = "listxattr",
	[__NR_SYSCALL_BASE + 233] = "llistxattr",
	[__NR_SYSCALL_BASE + 234] = "flistxattr",
	[__NR_SYSCALL_BASE + 235] = "removexattr",
	[__NR_SYSCALL_BASE + 236] = "lremovexattr",
	[__NR_SYSCALL_BASE + 237] = "fremovexattr",
	[__NR_SYSCALL_BASE + 238] = "tkill",
	[__NR_SYSCALL_BASE + 239] = "sendfile64",
	[__NR_SYSCALL_BASE + 240] = "futex",
	[__NR_SYSCALL_BASE + 241] = "sched_setaffinity",
	[__NR_SYSCALL_BASE + 242] = "sched_getaffinity",
	[__NR_SYSCALL_BASE + 243] = "io_setup",
	[__NR_SYSCALL_BASE + 244] = "io_destroy",
	[__NR_SYSCALL_BASE + 245] = "io_getevents",
	[__NR_SYSCALL_BASE + 246] = "io_submit",
	[__NR_SYSCALL_BASE + 247] = "io_cancel",
	[__NR_SYSCALL_BASE + 248] = "exit_group",
	[__NR_SYSCALL_BASE + 249] = "lookup_dcookie",
	[__NR_SYSCALL_BASE + 250] = "epoll_create",
	[__NR_SYSCALL_BASE + 251] = "epoll_ctl",
	[__NR_SYSCALL_BASE + 252] = "epoll_wait",
	[__NR_SYSCALL_BASE + 253] = "remap_file_pages",
	[__NR_SYSCALL_BASE + 256] = "set_tid_address",
	[__NR_SYSCALL_BASE + 257] = "timer_create",
	[__NR_SYSCALL_BASE + 258] = "timer_settime",
	[__NR_SYSCALL_BASE + 259] = "timer_gettime",
	[__NR_SYSCALL_BASE + 260] = "timer_getoverrun",
	[__NR_SYSCALL_BASE + 261] = "timer_delete",
	[__NR_SYSCALL_BASE + 262] = "clock_settime",
	[__NR_SYSCALL_BASE + 263] = "clock_gettime",
	[__NR_SYSCALL_BASE + 264] = "clock_getres",
	[__NR_SYSCALL_BASE + 265] = "clock_nanosleep",
	[__NR_SYSCALL_BASE + 266] = "statfs64",
	[__NR_SYSCALL_BASE + 267] = "fstatfs64",
	[__NR_SYSCALL_BASE + 268] = "tgkill",
	[__NR_SYSCALL_BASE + 269] = "utimes",
	[__NR_SYSCALL_BASE + 270] = "arm_fadvise64_64",
	[__NR_SYSCALL_BASE + 271] = "pciconfig_iobase",
	[__NR_SYSCALL_BASE + 272] = "pciconfig_read",
	[__NR_SYSCALL_BASE + 273] = "pciconfig_write",
	[__NR_SYSCALL_BASE + 274] = "mq_open",
	[__NR_SYSCALL_BASE + 275] = "mq_unlink",
	[__NR_SYSCALL_BASE + 276] = "mq_timedsend",
	[__NR_SYSCALL_BASE + 277] = "mq_timedreceive",
	[__NR_SYSCALL_BASE + 278] = "mq_notify",
	[__NR_SYSCALL_BASE + 279] = "mq_getsetattr",
	[__NR_SYSCALL_BASE + 280] = "waitid",
	[__NR_SYSCALL_BASE + 281] = "socket",
	[__NR_SYSCALL_BASE + 282] = "bind",
	[__NR_SYSCALL_BASE + 283] = "connect",
	[__NR_SYSCALL_BASE + 284] = "listen",
	[__NR_SYSCALL_BASE + 285] = "accept",
	[__NR_SYSCALL_BASE + 286] = "getsockname",
	[__NR_SYSCALL_BASE + 287] = "getpeername",
	[__NR_SYSCALL_BASE + 288] = "socketpair",
	[__NR_SYSCALL_BASE + 289] = "send",
	[__NR_SYSCALL_BASE + 290] = "sendto",
	[__NR_SYSCALL_BASE + 291] = "recv",
	[__NR_SYSCALL_BASE + 292] = "recvfrom",
	[__NR_SYSCALL_BASE + 293] = "shutdown",
	[__NR_SYSCALL_BASE + 294] = "setsockopt",
	[__NR_SYSCALL_BASE + 295] = "getsockopt",
	[__NR_SYSCALL_BASE + 296] = "sendmsg",
	[__NR_SYSCALL_BASE + 297] = "recvmsg",
	[__NR_SYSCALL_BASE + 298] = "semop",
	[__NR_SYSCALL_BASE + 299] = "semget",
	[__NR_SYSCALL_BASE + 300] = "semctl",
	[__NR_SYSCALL_BASE + 301] = "msgsnd",
	[__NR_SYSCALL_BASE + 302] = "msgrcv",
	[__NR_SYSCALL_BASE + 303] = "msgget",
	[__NR_SYSCALL_BASE + 304] = "msgctl",
	[__NR_SYSCALL_BASE + 305] = "shmat",
	[__NR_SYSCALL_BASE + 306] = "shmdt",
	[__NR_SYSCALL_BASE + 307] = "shmget",
	[__NR_SYSCALL_BASE + 308] = "shmctl",
	[__NR_SYSCALL_BASE + 309] = "add_key",
	[__NR_SYSCALL_BASE + 310] = "request_key",
	[__NR_SYSCALL_BASE + 311] = "keyctl",
	[__NR_SYSCALL_BASE + 312] = "semtimedop",
	[__NR_SYSCALL_BASE + 313] = "vserver",
	[__NR_SYSCALL_BASE + 314] = "ioprio_set",
	[__NR_SYSCALL_BASE + 315] = "ioprio_get",
	[__NR_SYSCALL_BASE + 316] = "inotify_init",
	[__NR_SYSCALL_BASE + 317] = "inotify_add_watch",
	[__NR_SYSCALL_BASE + 318] = "inotify_rm_watch",
	[__NR_SYSCALL_BASE + 319] = "mbind",
	[__NR_SYSCALL_BASE + 320] = "get_mempolicy",
	[__NR_SYSCALL_BASE + 321] = "set_mempolicy",
	[__NR_SYSCALL_BASE + 322] = "openat",
	[__NR_SYSCALL_BASE + 323] = "mkdirat",
	[__NR_SYSCALL_BASE + 324] = "mknodat",
	[__NR_SYSCALL_BASE + 325] = "fchownat",
	[__NR_SYSCALL_BASE + 326] = "futimesat",
	[__NR_SYSCALL_BASE + 327] = "fstatat64",
	[__NR_SYSCALL_BASE + 328] = "unlinkat",
	[__NR_SYSCALL_BASE + 329] = "renameat",
	[__NR_SYSCALL_BASE + 330] = "linkat",
	[__NR_SYSCALL_BASE + 331] = "symlinkat",
	[__NR_SYSCALL_BASE + 332] = "readlinkat",
	[__NR_SYSCALL_BASE + 333] = "fchmodat",
	[__NR_SYSCALL_BASE + 334] = "faccessat",
	[__NR_SYSCALL_BASE + 335] = "pselect6",
	[__NR_SYSCALL_BASE + 336] = "ppoll",
	[__NR_SYSCALL_BASE + 337] = "unshare",
	[__NR_SYSCALL_BASE + 338] = "set_robust_list",
	[__NR_SYSCALL_BASE + 339] = "get_robust_list",
	[__NR_SYSCALL_BASE + 340] = "splice",
	[__NR_SYSCALL_BASE + 341] = "arm_sync_file_range",
	[__NR_SYSCALL_BASE + 342] = "tee",
	[__NR_SYSCALL_BASE + 343] = "vmsplice",
	[__NR_SYSCALL_BASE + 344] = "move_pages",
	[__NR_SYSCALL_BASE + 345] = "getcpu",
	[__NR_SYSCALL_BASE + 346] = "epoll_pwait",
	[__NR_SYSCALL_BASE + 347] = "kexec_load",
	[__NR_SYSCALL_BASE + 348] = "utimensat",
	[__NR_SYSCALL_BASE + 349] = "signalfd",
	[__NR_SYSCALL_BASE + 350] = "timerfd_create",
	[__NR_SYSCALL_BASE + 351] = "eventfd",
	[__NR_SYSCALL_BASE + 352] = "fallocate",
	[__NR_SYSCALL_BASE + 353] = "timerfd_settime",
	[__NR_SYSCALL_BASE + 354] = "timerfd_gettime",
	[__NR_SYSCALL_BASE + 355] = "signalfd4",
	[__NR_SYSCALL_BASE + 356] = "eventfd2",
	[__NR_SYSCALL_BASE + 357] = "epoll_create1",
	[__NR_SYSCALL_BASE + 358] = "dup3",
	[__NR_SYSCALL_BASE + 359] = "pipe2",
	[__NR_SYSCALL_BASE + 360] = "inotify_init1",
	[__NR_SYSCALL_BASE + 361] = "preadv",
	[__NR_SYSCALL_BASE + 362] = "pwritev",
	[__NR_SYSCALL_BASE + 363] = "rt_tgsigqueueinfo",
	[__NR_SYSCALL_BASE + 364] = "perf_event_open",
	[__NR_SYSCALL_BASE + 365] = "recvmmsg",
	[__NR_SYSCALL_BASE + 366] = "accept4",
	[__NR_SYSCALL_BASE + 367] = "fanotify_init",
	[__NR_SYSCALL_BASE + 368] = "fanotify_mark",
	[__NR_SYSCALL_BASE + 369] = "prlimit64",
	[__NR_SYSCALL_BASE + 370] = "name_to_handle_at",
	[__NR_SYSCALL_BASE + 371] = "open_by_handle_at",
	[__NR_SYSCALL_BASE + 372] = "clock_adjtime",
	[__NR_SYSCALL_BASE + 373] = "syncfs",
	[__NR_SYSCALL_BASE + 374] = "sendmmsg",
	[__NR_SYSCALL_BASE + 375] = "setns",
	[__NR_SYSCALL_BASE + 376] = "process_vm_readv",
	[__NR_SYSCALL_BASE + 377] = "process_vm_writev",
	[__NR_SYSCALL_BASE + 378] = "kcmp",
	[__NR_SYSCALL_BASE + 379] = "finit_module",
	[__NR_SYSCALL_BASE + 380] = "sched_setattr",
	[__NR_SYSCALL_BASE + 381] = "sched_getattr",
	[__NR_SYSCALL_BASE + 382] = "renameat2",
	[__NR_SYSCALL_BASE + 383] = "seccomp",
	[__NR_SYSCALL_BASE + 384] = "getrandom",
	[__NR_SYSCALL_BASE + 385] = "memfd_create",
	[__NR_SYSCALL_BASE + 386] = "bpf",
	[__NR_SYSCALL_BASE + 387] = "execveat",
	[__NR_SYSCALL_BASE + 388] = "userfaultfd",
	[__NR_SYSCALL_BASE + 389] = "membarrier",
	[__NR_SYSCALL_BASE + 390] = "mlock2",
	[__NR_SYSCALL_BASE + 391] = "copy_file_range",
	[__NR_SYSCALL_BASE + 392] = "preadv2",
	[__NR_SYSCALL_BASE + 393] = "pwritev2",
	[__NR_SYSCALL_BASE + 394] = "pkey_mprotect",
	[__NR_SYSCALL_BASE + 395] = "pkey_alloc",
	[__NR_SYSCALL_BASE + 396] = "pkey_free",
	[__NR_SYSCALL_BASE + 397] = "statx",
};

static inline const char *
get_syscall_name(long nr)
{
	if (nr >= (long)ARRAY_SIZE(syscall_names))
		return NULL;
	else
		return syscall_names[nr];
}

static const char *pkt7_names[] = {
	[CP_ME_INIT]			= "CP_ME_INIT",
	[CP_NOP]			= "CP_NOP",
	[CP_PREEMPT_TOKEN]		= "CP_PREEMPT_TOKEN",
	[CP_INDIRECT_BUFFER]		= "CP_INDIRECT_BUFFER",
	[CP_INDIRECT_BUFFER_PFD]	= "CP_INDIRECT_BUFFER_PFD",
	[CP_WAIT_FOR_IDLE]		= "CP_WAIT_FOR_IDLE",
	[CP_WAIT_REG_MEM]		= "CP_WAIT_REG_MEM",
	[CP_WAIT_REG_EQ]		= "CP_WAIT_REG_EQ",
	[CP_WAIT_IB_PFD_COMPLETE]	= "CP_WAIT_IB_PFD_COMPLETE",
	[CP_REG_RMW]			= "CP_REG_RMW",
	[CP_SET_BIN_DATA]		= "CP_SET_BIN_DATA",
	[CP_REG_TO_MEM]			= "CP_REG_TO_MEM",
	[CP_MEM_WRITE]			= "CP_MEM_WRITE",
	[CP_MEM_WRITE_CNTR]		= "CP_MEM_WRITE_CNTR",
	[CP_COND_EXEC]			= "CP_COND_EXEC",
	[CP_COND_WRITE]			= "CP_COND_WRITE",
	[CP_EVENT_WRITE]		= "CP_EVENT_WRITE",
	[CP_EVENT_WRITE_SHD]		= "CP_EVENT_WRITE_SHD",
	[CP_EVENT_WRITE_CFL]		= "CP_EVENT_WRITE_CFL",
	[CP_EVENT_WRITE_ZPD]		= "CP_EVENT_WRITE_ZPD",
	[CP_RUN_OPENCL]			= "CP_RUN_OPENCL",
	[CP_DRAW_INDX]			= "CP_DRAW_INDX",
	[CP_SET_STATE]			= "CP_SET_STATE",
	[CP_SET_CONSTANT]		= "CP_SET_CONSTANT",
	[CP_IM_LOAD]			= "CP_IM_LOAD",
	[CP_IM_LOAD_IMMEDIATE]		= "CP_IM_LOAD_IMMEDIATE",
	[CP_LOAD_CONSTANT_CONTEXT]	= "CP_LOAD_CONSTANT_CONTEXT",
	[CP_INVALIDATE_STATE]		= "CP_INVALIDATE_STATE",
	[CP_SET_BIN_SELECT]		= "CP_SET_BIN_SELECT",
	[CP_CONTEXT_UPDATE]		= "CP_CONTEXT_UPDATE",
	[CP_INTERRUPT]			= "CP_INTERRUPT",
	[CP_SET_DRAW_INIT_FLAGS]	= "CP_SET_DRAW_INIT_FLAGS",
	[CP_SET_PROTECTED_MODE]		= "CP_SET_PROTECTED_MODE",
	[CP_BOOTSTRAP_UCODE]		= "CP_BOOTSTRAP_UCODE",
	[CP_LOAD_STATE]			= "CP_LOAD_STATE",
	[CP_SET_BIN]			= "CP_SET_BIN",
	[CP_TEST_TWO_MEMS]		= "CP_TEST_TWO_MEMS",
	[CP_REG_WR_NO_CTXT]		= "CP_REG_WR_NO_CTXT",
	[CP_RECORD_PFP_TIMESTAMP]	= "CP_RECORD_PFP_TIMESTAMP",
	[CP_SET_SECURE_MODE]		= "CP_SET_SECURE_MODE",
	[CP_WAIT_FOR_ME]		= "CP_WAIT_FOR_ME",
	[CP_SET_DRAW_STATE]		= "CP_SET_DRAW_STATE",
	[CP_DRAW_INDX_OFFSET]		= "CP_DRAW_INDX_OFFSET",
	[CP_DRAW_INDIRECT]		= "CP_DRAW_INDIRECT",
	[CP_DRAW_INDX_INDIRECT]		= "CP_DRAW_INDX_INDIRECT",
	[CP_DRAW_AUTO]			= "CP_DRAW_AUTO",
	[CP_UNKNOWN_19]			= "CP_UNKNOWN_19",
	[CP_UNKNOWN_1A]			= "CP_UNKNOWN_1A",
	[CP_UNKNOWN_4E]			= "CP_UNKNOWN_4E",
	[CP_WIDE_REG_WRITE]		= "CP_WIDE_REG_WRITE",
	[CP_SCRATCH_TO_REG]		= "CP_SCRATCH_TO_REG",
	[CP_REG_TO_SCRATCH]		= "CP_REG_TO_SCRATCH",
	[CP_WAIT_MEM_WRITES]		= "CP_WAIT_MEM_WRITES",
	[CP_COND_REG_EXEC]		= "CP_COND_REG_EXEC",
	[CP_MEM_TO_REG]			= "CP_MEM_TO_REG",
	[CP_EXEC_CS_INDIRECT]		= "CP_EXEC_CS_INDIRECT",
	[CP_EXEC_CS]			= "CP_EXEC_CS",
	[CP_PERFCOUNTER_ACTION]		= "CP_PERFCOUNTER_ACTION",
	[CP_SMMU_TABLE_UPDATE]		= "CP_SMMU_TABLE_UPDATE",
	[CP_SET_MARKER]			= "CP_SET_MARKER",
	[CP_SET_PSEUDO_REG]		= "CP_SET_PSEUDO_REG",
	[CP_CONTEXT_REG_BUNCH]		= "CP_CONTEXT_REG_BUNCH",
	[CP_YIELD_ENABLE]		= "CP_YIELD_ENABLE",
	[CP_SKIP_IB2_ENABLE_GLOBAL]	= "CP_SKIP_IB2_ENABLE_GLOBAL",
	[CP_SKIP_IB2_ENABLE_LOCAL]	= "CP_SKIP_IB2_ENABLE_LOCAL",
	[CP_SET_SUBDRAW_SIZE]		= "CP_SET_SUBDRAW_SIZE",
	[CP_SET_VISIBILITY_OVERRIDE]	= "CP_SET_VISIBILITY_OVERRIDE",
	[CP_PREEMPT_ENABLE_GLOBAL]	= "CP_PREEMPT_ENABLE_GLOBAL",
	[CP_PREEMPT_ENABLE_LOCAL]	= "CP_PREEMPT_ENABLE_LOCAL",
	[CP_CONTEXT_SWITCH_YIELD]	= "CP_CONTEXT_SWITCH_YIELD",
	[CP_SET_RENDER_MODE]		= "CP_SET_RENDER_MODE",
	[CP_COMPUTE_CHECKPOINT]		= "CP_COMPUTE_CHECKPOINT",
	[CP_MEM_TO_MEM]			= "CP_MEM_TO_MEM",
	[CP_BLIT]			= "CP_BLIT",
	[CP_REG_TEST]			= "CP_REG_TEST",
	[CP_SET_MODE]			= "CP_SET_MODE",
	[CP_LOAD_STATE6_GEOM]		= "CP_LOAD_STATE6_GEOM",
	[CP_LOAD_STATE6_FRAG]		= "CP_LOAD_STATE6_FRAG",
	[CP_LOAD_STATE6]		= "CP_LOAD_STATE6",
	[CP_UNK_A6XX_14]		= "CP_UNK_A6XX_14",
	[CP_UNK_A6XX_55]		= "CP_UNK_A6XX_55",
	[CP_REG_WRITE]			= "CP_REG_WRITE",
};

static inline const char *
get_pkt7_name(long nr)
{
	if (nr >= (long)ARRAY_SIZE(pkt7_names))
		return NULL;
	else
		return pkt7_names[nr];
}
