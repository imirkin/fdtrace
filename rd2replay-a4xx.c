#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <sys/ptrace.h>
#include <asm/ptrace.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <assert.h>

#define ARRAY_LENGTH(x) (sizeof(x) / sizeof((x)[0]))

#include "regs4.h"
#include "disasm.h"

uint32_t debug;

static void __attribute__((noreturn))
fail(const char *fmt, ...)
{
	va_list va;

	va_start(va, fmt);
	vfprintf(stderr, fmt, va);
	fprintf(stderr, "\n");
	__builtin_unreachable();
}

static void
fail_if(int cond, const char *fmt, ...)
{
	va_list va;

	if (cond) {
		va_start(va, fmt);
		vfprintf(stderr, fmt, va);
		fprintf(stderr, "\n");
		va_end(va);
		raise(SIGTRAP);
	}
}

static void *
xmalloc(size_t size)
{
	void *p = malloc(size);

	fail_if(p == NULL, "malloc failed");

	return p;
}

static void
xfread(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
	size_t n = fread(ptr, size, nmemb, stream);

	fail_if(n != nmemb,
		"read failed or short, got %d of %d expected bytes", n, size);
}

enum rd_sect_type {
	RD_NONE,
	RD_TEST,       /* ascii text */
	RD_CMD,        /* ascii text */
	RD_GPUADDR,    /* u32 gpuaddr, u32 size */
	RD_CONTEXT,    /* raw dump */
	RD_CMDSTREAM,  /* raw dump */
	RD_CMDSTREAM_ADDR, /* gpu addr of cmdstream */
	RD_PARAM,      /* u32 param_type, u32 param_val, u32 bitlen */
	RD_FLUSH,      /* empty, clear previous params */
	RD_PROGRAM,    /* shader program, raw dump */
	RD_VERT_SHADER,
	RD_FRAG_SHADER,
	RD_BUFFER_CONTENTS,
	RD_GPU_ID,
};

static struct bo {
	uint64_t iova;
	uint32_t size;
	void *contents;
} replay_bos[256];
static int num_bos;

static uint64_t streams[1024];
static uint32_t num_streams;

static bool did_stream(uint64_t s)
{
	for (uint32_t i = 0; i < num_streams; i++)
		if (streams[i] == s)
			return true;

	return false;
}

static void add_stream(uint64_t s)
{
	if (!did_stream(s)) {
		fail_if(num_streams == ARRAY_LENGTH(streams), "too many streams");
		streams[num_streams++] = s;
	}
}

static struct bo *
find_bo(uint64_t start)
{
	for (uint32_t i = 0; i < num_bos; i++) {
		if (replay_bos[i].iova <= start && start < replay_bos[i].iova + replay_bos[i].size)
			return &replay_bos[i];
	}

	return NULL;
}

static inline uint64_t
get_offset(const uint32_t *p)
{
	return (uint64_t) p[0] & ~3; // | ((uint64_t) p[1] << 32);
}

static void __attribute__ ((format(__printf__, 4, 5)))
create_blob(uint64_t start, uint32_t size, uint32_t align, const char *msg, ...)
{
	va_list va;
	char annotation[256];

	if (did_stream(start))
		return;

	add_stream(start);

	va_start(va, msg);
	vsnprintf(annotation, sizeof(annotation), msg, va);
	va_end(va);

	printf("static struct stream stream_0x%lx;\n", start);
	printf("static void\nbuild_stream_0x%lx(struct allocator *cs)\n{\n", start);
	printf("\t/* %s, %d bytes */\n\n", annotation, size);

	struct bo *bo = find_bo(start);
	if (bo) {
		fail_if(size & 3, "only dword aligned blobs");

		uint32_t *dwords = bo->contents + (start - bo->iova);
		uint32_t length = size / 4;
		printf("\tstatic const uint32_t contents[] = {\n");
		uint32_t i = 0;
		while (i < length) {
			uint32_t line_end = i + 8 < length ? i + 8 : length;

			printf("\t\t");
			while (i < line_end)
				printf("0x%08x, ", dwords[i++]);
			printf("\n");
		}
		printf("\t};\n\n");

		printf("\tstream_0x%lx = allocate_block(cs, contents, sizeof(contents), %d);\n}\n\n",
		       start, align);
	} else {
		printf("\tstream_0x%lx = allocate_block(cs, NULL, %d, %d);\n}\n\n",
		       start, size, align);
	}
}

static const uint32_t tess_factor_size = 1024;

static void __attribute__ ((format(__printf__, 4, 5)))
create_tess_shader_constants(uint64_t start, uint32_t size, uint32_t align, const char *msg, ...)
{
	va_list va;
	char annotation[256];

	if (did_stream(start))
		return;

	add_stream(start);

	va_start(va, msg);
	vsnprintf(annotation, sizeof(annotation), msg, va);
	va_end(va);

	struct bo *bo = find_bo(start);
	fail_if(bo == NULL, "need bo for shader constants");
	fail_if(size & 3, "only dword aligned blobs");
	uint32_t *dwords = bo->contents + (start - bo->iova);
	uint32_t length = size / 4;
	fail_if (length < 4, "test constant buffer should be at least 4 dwords");

	uint64_t tess_param_offset = get_offset(&dwords[0]);
	create_blob(tess_param_offset, 1024, 4, "tess param buffer");
	uint64_t tess_factor_offset = get_offset(&dwords[2]);
	if (tess_factor_offset != 0)
		create_blob(tess_factor_offset, tess_factor_size, 4, "tess factor buffer");

	printf("static struct stream stream_0x%lx;\n", start);
	printf("static void\nbuild_stream_0x%lx(struct allocator *cs)\n{\n", start);
	printf("\t/* %s, %d bytes */\n\n", annotation, size);
	printf("\tconst uint32_t contents[] = {\n");
	printf("\t\tstream_0x%lx.offset, stream_0x%lx.offset >> 32,\n",
	       tess_param_offset, tess_param_offset);

	if (tess_factor_offset != 0) {
		printf("\t\tstream_0x%lx.offset, stream_0x%lx.offset >> 32,\n",
		       tess_factor_offset, tess_factor_offset);
	} else {
		printf("\t\t0, 0,\n");
	}

	uint32_t i = 4;
	while (i < length) {
		uint32_t line_end = i + 8 < length ? i + 8 : length;

		printf("\t\t");
		while (i < line_end)
			printf("0x%08x, ", dwords[i++]);
		printf("\n");
	}
	printf("\t};\n\n");

	printf("\tstream_0x%lx = allocate_block(cs, contents, sizeof(contents), %d);\n}\n\n",
	       start, align);
}


struct cp_load_state4 {
	uint32_t dst_off;
	uint32_t state_type;
	uint32_t state_src;
	uint32_t state_block;
	uint32_t num_unit;
	uint64_t address;
};

static inline uint32_t
field(uint32_t value, int start, int end)
{
	uint32_t mask;

	mask = ~0U >> (31 - end + start);

	return (value >> start) & mask;
}

static inline uint32_t
clear_field(uint32_t value, int start, int end)
{
	uint32_t mask;

	mask = (~0U >> (31 - end + start)) << start;

	return value & ~mask;
}

static inline struct cp_load_state4
decode_cp_load_state4(const uint32_t *dw)
{
	return (struct cp_load_state4) {
		.dst_off	= field(dw[1],  0, 13),
		.state_type	= field(dw[2],  0,  1),
		.state_src	= field(dw[1], 16, 17),
		.state_block	= field(dw[1], 18, 21),
		.num_unit	= field(dw[1], 22, 31),
		.address	= get_offset(&dw[2])
	};
}

enum { TEXTURES, CONSTANTS, SAMPLERS, SHADER, SSBO_0, SSBO_1 };

static uint32_t classify_state(uint32_t state_type, uint32_t state_block)
{
	switch (state_type) {
	case ST4_CONSTANTS:
		switch (state_block) {
		case SB4_VS_TEX:
		case SB4_HS_TEX:
		case SB4_DS_TEX:
		case SB4_GS_TEX:
		case SB4_FS_TEX:
		case SB4_CS_TEX:
			return TEXTURES;
		case SB4_VS_SHADER:
		case SB4_HS_SHADER:
		case SB4_DS_SHADER:
		case SB4_GS_SHADER:
		case SB4_FS_SHADER:
		case SB4_CS_SHADER:
			return CONSTANTS;
		case SB4_SSBO:
		case SB4_CS_SSBO:
			return SSBO_0;
		default:
			fail("unhandled state block: %d", state_block);
		}
		break;
	case ST4_SHADER:
		switch (state_block) {
		case SB4_VS_TEX:
		case SB4_HS_TEX:
		case SB4_DS_TEX:
		case SB4_GS_TEX:
		case SB4_FS_TEX:
		case SB4_CS_TEX:
			return SAMPLERS;
		case SB4_VS_SHADER:
		case SB4_HS_SHADER:
		case SB4_DS_SHADER:
		case SB4_GS_SHADER:
		case SB4_FS_SHADER:
		case SB4_CS_SHADER:
			return SHADER;
		case SB4_SSBO:
		case SB4_CS_SSBO:
			return SSBO_1;
		default:
			fail("unhandled state block: %d", state_block);
		}
	default:
		fail("unhandled state type");
	}
}


static uint32_t dwords_per_unit(uint32_t state_type, uint32_t state_block)
{
	switch (classify_state(state_type, state_block)) {
	case TEXTURES:
		return 8;
	case CONSTANTS:
		return 4;
	case SAMPLERS:
		return 2;
	case SHADER:
		return 32;
	case SSBO_0:
		return 2;
	case SSBO_1:
		return 4;
	default:
		fail("unhandled state type");
	}
}

static const char *
stage_name(uint32_t state_block)
{
	switch (state_block) {
	case SB4_VS_TEX:
	case SB4_VS_SHADER:
		return "vertex";
	case SB4_HS_TEX:
	case SB4_HS_SHADER:
		return "hull";
	case SB4_DS_TEX:
	case SB4_DS_SHADER:
		return "domain";
	case SB4_GS_TEX:
	case SB4_GS_SHADER:
		return "geometry";
	case SB4_FS_TEX:
	case SB4_FS_SHADER:
		return "fragment";
	case SB4_CS_TEX:
	case SB4_CS_SHADER:
		return "compute";
	default:
		fail("unhandled state block: %d", state_block);
	}
}

static void
create_textures_state(uint32_t *dwords)
{
	const struct cp_load_state4 state = decode_cp_load_state4(dwords);

	if (did_stream(state.address))
		return;

	add_stream(state.address);

	printf("static struct stream stream_0x%lx;\n", state.address);
	printf("static void\nbuild_stream_0x%lx(struct allocator *cs)\n{\n", state.address);
	printf("\t/* %s texture state */\n\n", stage_name(state.state_block));
}

static void
create_samplers_state(uint32_t *dwords)
{
	const struct cp_load_state4 state = decode_cp_load_state4(dwords);

	if (did_stream(state.address))
		return;

	add_stream(state.address);

	printf("static struct stream stream_0x%lx;\n", state.address);
	printf("static void\nbuild_stream_0x%lx(struct allocator *cs)\n{\n", state.address);
	printf("\t/* %s sampler state */\n\n", stage_name(state.state_block));
}

static void
create_shader_state(uint32_t *dwords)
{
	const struct cp_load_state4 state = decode_cp_load_state4(dwords);

	if (did_stream(state.address))
		return;

	add_stream(state.address);

	printf("static struct stream stream_0x%lx;\n", state.address);
	printf("static void\nbuild_stream_0x%lx(struct allocator *cs)\n{\n", state.address);
	printf("\t/* %s shader instructions */\n\n", stage_name(state.state_block));

	struct bo *bo = find_bo(state.address);
	if (bo) {
		uint32_t *instrs = bo->contents + (state.address - bo->iova);
		uint32_t length = state.num_unit * 32;

		begin_disasm(420);

		printf("\tstatic const uint64_t contents[] = {\n");
		bool end = false;
		for (uint32_t i = 0; i < length; i += 2) {
			printf("\t\t0x%08x%08xul,", instrs[i + 1], instrs[i]);
			if (!end) {
				printf("\t// ");
				end = disasm_a3xx(&instrs[i], i);
			} else {
				printf("\n");
			}
		}

		printf("\n");
		a3xx_print_reg_stats();

		printf("\t};\n\n");
	}

	printf("\tstream_0x%lx = allocate_block(cs, contents, sizeof(contents), %d);\n}\n\n",
	       state.address, 16);
}

struct regs {
	uint32_t value[0x10000];
	bool written[0x10000];
};

static inline void
write_reg(struct regs *regs, uint32_t offset, uint32_t value)
{
	regs->value[offset] = value;
	regs->written[offset] = true;
}

static void __attribute__ ((format(__printf__, 3, 4)))
dump_cmdstream(uint64_t start, uint32_t count, const char *msg, ...)
{
	va_list va;
	char annotation[256];

	if (did_stream(start))
		return;

	va_start(va, msg);
	vsnprintf(annotation, sizeof(annotation), msg, va);
	va_end(va);

	struct bo *bo = find_bo(start);
	if (bo == NULL) {
		printf("\t/* ----- no bo for cmd stream at 0x%lx, length %d (%s)\n", start, count, annotation);
		return;
	}

	fail_if(bo == NULL, "no bo for cmdstream at %lx (%s)\n", start, annotation);

	struct regs regs = { };

	uint32_t *dwords = bo->contents + (start - bo->iova);

	for (uint32_t i = 0; i < count; ) {
		uint32_t pkt = field(dwords[i], 30, 31);
		uint32_t cnt = field(dwords[i], 16, 22) + 1;

		switch (pkt) {
		case 3: {
			uint32_t opc = (dwords[i] >> 8) & 0x7f;
			/* NOTE: copied from a6xx, not all have been
			 * audited for correctness/existence on a4xx. */
			switch (opc) {
			case CP_MEM_WRITE: {
				uint64_t offset = get_offset(&dwords[i + 1]);
				create_blob(offset, 16, 16, "CP_MEM_WRITE from stream 0x%lx", start);
				break;
			}
			case CP_EVENT_WRITE: {
				if (cnt == 1)
					break;
				uint64_t offset = get_offset(&dwords[i + 2]);
				create_blob(offset, 4, 4, "CP_EVENT_WRITE from stream 0x%lx", start);
				break;
			}
			case CP_MEM_TO_REG: {
				uint64_t offset = get_offset(&dwords[i + 2]);
				uint32_t length = field(dwords[i + 1], 19, 29);
				create_blob(offset, length * 4, 16, "CP_MEM_TO_REG from stream 0x%lx", start);
				break;
			}
			case CP_INDIRECT_BUFFER: {
				uint64_t offset = get_offset(&dwords[i + 1]);
				uint32_t length = dwords[i + 2];
				dump_cmdstream(offset, length, "CP_INDIRECT_BUFFER from stream 0x%lx", start);
				break;
			}
			case CP_DRAW_INDX_OFFSET: {
				if (cnt == 3)
					break;
				uint64_t offset = get_offset(&dwords[i + 5]);
				uint32_t size = dwords[6];
				create_blob(offset, size, 4, "index buffer from stream 0x%lx", start);
				break;
			}
			case CP_SET_DRAW_STATE: {
				for (uint32_t j = 0; j < cnt; j += 2) {
					uint32_t length = field(dwords[i + 1 + j], 0, 15);
					uint32_t group_id = field(dwords[i + 1 + j], 24, 28);
					uint64_t offset = get_offset(&dwords[i + 2 + j]);
					if (length > 0) {
						dump_cmdstream(offset, length,
							       "CP_SET_DRAW_STATE group %d from stream 0x%lx",
							       group_id, start);
					}

				}
				break;
			}

			case CP_LOAD_STATE4: {
				const struct cp_load_state4 state = decode_cp_load_state4(&dwords[i]);
				if (state.state_src == SS4_DIRECT)
					break;

				uint32_t size = dwords_per_unit(state.state_type, state.state_block) *
					state.num_unit * 4;
				switch (classify_state(state.state_type, state.state_block)) {
				/*case TEXTURES:
					create_textures_state(&dwords[i]);
					break;
				*/
				case SHADER:
					create_shader_state(&dwords[i]);
					break;
				/*case SAMPLERS:
					create_samplers_state(&dwords[i]);
					break;
				*/
				case CONSTANTS:
					if ((state.state_block == SB4_HS_SHADER ||
					     state.state_block == SB4_DS_SHADER) &&
					    state.dst_off == 8)
						create_tess_shader_constants(state.address, size, 64,
									     "%s shader constants (dst_off %d) from stream 0x%lx",
									     stage_name(state.state_block), state.dst_off, start);
					else
						create_blob(state.address, size, 64,
							    "%s shader constants (dst_off %d) from stream 0x%lx",
							    stage_name(state.state_block), state.dst_off, start);
					break;
				default:
					create_blob(state.address, size, 64,
						    "state type %d, block %d (dst_off %d) from stream 0x%lx",
						    state.state_type, state.state_block, state.dst_off, start);
					break;
				}
				break;
			}
			}
		}
		case 0: {
			uint32_t reg_base = (dwords[i]) & 0x7fff;
			for (uint32_t j = 0; j < cnt; j++) {
				uint32_t reg = reg_base + j;
				uint32_t value = dwords[i + 1 + j];
				write_reg(&regs, reg, value);
			}
		}
		default:
			break;
		}
		i += cnt + 1;
	}

	static const struct { uint32_t base, len, factor; }  address_regs[] = {
		/* Copied from the a6xx variant, but most of these
		 * could be hooked up easily. Do so as needed. */
/*
		{ RB_MRT0_BASE_LO, RB_MRT0_ARRAY_PITCH, 1 }, // Using ARRAY_PITCH kinda works...
		{ RB_MRT1_BASE_LO, RB_MRT1_ARRAY_PITCH, 1 },
		{ RB_MRT2_BASE_LO, RB_MRT2_ARRAY_PITCH, 1 },
		{ RB_MRT3_BASE_LO, RB_MRT3_ARRAY_PITCH, 1 },
		{ RB_MRT4_BASE_LO, RB_MRT4_ARRAY_PITCH, 1 },
		{ RB_MRT5_BASE_LO, RB_MRT5_ARRAY_PITCH, 1 },
		{ RB_MRT6_BASE_LO, RB_MRT6_ARRAY_PITCH, 1 },
		{ RB_MRT7_BASE_LO, RB_MRT7_ARRAY_PITCH, 1 },
		{ RB_DEPTH_BUFFER_BASE_LO, RB_DEPTH_BUFFER_ARRAY_PITCH, 1 },
		{ RB_STENCIL_BUFFER_BASE_LO, RB_STENCIL_BUFFER_ARRAY_PITCH, 1 },

		{ SP_PS_2D_SRC_LO, SP_PS_2D_SRC_SIZE, 1 },
		{ RB_2D_DST_LO, RB_2D_DST_SIZE, 1 },

		{ PC_TESSFACTOR_ADDR_LO, 0, 1 },
*/
		// xxx SP_FS_TEX_COUNT etc

		{ SP_VS_OBJ_START, HLSQ_VS_CONTROL_REG, 32 * 4 },
		{ SP_HS_OBJ_START, HLSQ_HS_CONTROL_REG, 32 * 4 },
		{ SP_DS_OBJ_START, HLSQ_DS_CONTROL_REG, 32 * 4 },
		{ SP_GS_OBJ_START, HLSQ_GS_CONTROL_REG, 32 * 4 },
		{ SP_FS_OBJ_START, HLSQ_FS_CONTROL_REG, 32 * 4 },
		{ SP_CS_OBJ_START, HLSQ_CS_CONTROL_REG, 32 * 4 },
/*
		{ VFD_FETCH0_BASE, VFD_FETCH0_SIZE, 1 },
		{ VFD_FETCH1_BASE, VFD_FETCH1_SIZE, 1 },
		{ VFD_FETCH2_BASE, VFD_FETCH2_SIZE, 1 },
		{ VFD_FETCH3_BASE, VFD_FETCH3_SIZE, 1 },
		{ VFD_FETCH4_BASE, VFD_FETCH4_SIZE, 1 },
		{ VFD_FETCH6_BASE, VFD_FETCH5_SIZE, 1 },
		{ VFD_FETCH7_BASE, VFD_FETCH6_SIZE, 1 },
*/
	};

	for (uint32_t i = 0; i < ARRAY_LENGTH(address_regs); i++) {
		uint32_t lo = address_regs[i].base;
		uint32_t len = address_regs[i].len;

		if (regs.written[lo]) {
			fail_if(!regs.written[lo],
				"expect all of lo and len to be written");
			fail_if(len && !regs.written[len],
				"expect all of lo and len to be written");

			uint64_t offset = regs.value[lo] & ~0xf;
			uint32_t size, stride, y;
			switch (address_regs[i].base) {
/*
			case RB_2D_DST_LO:
				stride = regs.value[len] << 6;
				y = field(regs.value[GRAS_2D_DST_BR], 16, 30);
				size = stride * (y + 1);
				break;
			case PC_TESSFACTOR_ADDR_LO:
				size = tess_factor_size;
				break;
*/
			default:
				size = (regs.value[len] >> 24) * address_regs[i].factor;
				break;
			}
			if (size == 0)
				continue;
			const char *name = lookup_register(lo);
			create_blob(offset, size, 64, "%s from stream 0x%lx", name, start);
		}
	}

	add_stream(start);

	printf("static struct stream stream_0x%lx;\n", start);
	printf("static void\nbuild_stream_0x%lx(struct allocator *cs)\n{\n", start);
	printf("\t/* %s, %d dwords */\n\n", annotation, count);

	printf("\tbegin_stream(cs, 4);\n\n");

	for (uint32_t i = 0; i < count; ) {
		uint32_t pkt = dwords[i] >> 30;
		uint32_t cnt = field(dwords[i], 16, 22) + 1;

		switch (pkt) {
		case 3: {
			uint32_t opc = (dwords[i] >> 8) & 0x7f;
			const char *name = lookup_pkt7_name(opc);

			/* NOTE: copied from a6xx, not all have been
			 * audited for correctness/existence on a4xx. */
			switch (opc) {
			case CP_MEM_WRITE: {
				uint64_t offset = get_offset(&dwords[i + 1]);
				printf("\tcp(cs, %s, stream_0x%lx.offset, stream_0x%lx.offset >> 32, 0x%x);\n",
				       name, offset, offset, dwords[i + 3]);
				break;
			}
			case CP_EVENT_WRITE: {
				if (cnt == 1) {
					printf("\tcp(cs, %s, 0x%x);\n", name, dwords[i + 1]);
				} else {
					uint64_t offset = get_offset(&dwords[i + 2]);
					printf("\tcp(cs, %s, 0x%x, stream_0x%lx.offset, stream_0x%lx.offset >> 32, 0x%x);\n",
					       name, dwords[i + 1], offset, offset, dwords[i + 4]);
				}
				break;
			}
			case CP_MEM_TO_REG: {
				uint64_t offset = get_offset(&dwords[i + 2]);
				printf("\tcp(cs, %s, 0x%x | ((stream_0x%lx.size / 4) << 19),\n"
				       "\t   stream_0x%lx.offset, stream_0x%lx.offset >> 32);\n",
				       name, clear_field(dwords[i + 1], 19, 29), offset, offset, offset);
				break;
			}
			case CP_INDIRECT_BUFFER: {
				uint64_t offset = get_offset(&dwords[i + 1]);
				printf("\tib(cs, stream_0x%lx);\n", offset);
				break;
			}
			case CP_DRAW_INDX_OFFSET: {
				if (cnt == 2) {
					printf("\tcp(cs, %s, 0x%x, 0x%x, 0x%x);\n",
					       name, dwords[i + 1], dwords[i + 2], 0);
				} else {
					uint64_t offset = get_offset(&dwords[i + 5]);
					printf("\tcp(cs, %s, 0x%x, 0x%x, 0x%x,\n"
					       "\t   stream_0x%lx.offset, stream_0x%lx.offset >> 32, stream_0x%lx.size);\n",
					       name, dwords[i + 1], dwords[i + 2], dwords[i + 3],
					       offset, offset, offset);
				}
				break;
			}
			case CP_SET_DRAW_STATE: {
				printf("\tcp(cs, %s", name);
				for (uint32_t j = 0; j < cnt; j += 2) {
					printf(",\n\t   ");

					uint32_t length = dwords[i + 1 + j] & 0xffff;
					uint64_t offset = get_offset(&dwords[i + 2 + j]);

					if (length > 0) {
						printf("group(0x%x, stream_0x%lx)",
						       dwords[i + 1 + j] & ~0xffff, offset);
					} else {
						printf("0x%08x, 0x%08x, 0x%08x",
						       dwords[i + 1 + j], dwords[i + 2 + j], dwords[i + 3 + j]);
					}
				}
				printf(");\n");
				break;
			}
			case CP_LOAD_STATE4: {
				const struct cp_load_state4 state = decode_cp_load_state4(&dwords[i]);

				printf("\tcp(cs, %s", name);
				if (state.state_src == SS4_INDIRECT) {
					uint64_t offset = get_offset(&dwords[i + 2]);
					uint32_t bytes_per_unit = dwords_per_unit(state.state_type, state.state_block) * 4;
					printf(", 0x%08x | DIV_ROUND_UP(stream_0x%lx.size, %d) << 22, 0x%08x | stream_0x%lx.offset", //", stream_0x%lx.offset >> 32",
					       clear_field(dwords[i + 1], 22, 31), offset, bytes_per_unit,
					       clear_field(dwords[i + 2], 2, 31), offset);
				} else {
					for (uint32_t j = 0; j < cnt; j++)
						printf(", 0x%08x", dwords[i + 1 + j]);
				}
				printf(");\n");
				break;
			}
			default: {
				if (name)
					printf("\tcp(cs, %s", name);
				else
					printf("\tcp(cs, %d", opc);

				for (uint32_t j = 0; j < cnt; j++)
					printf(", 0x%08x", dwords[i + 1 + j]);
				printf(");\n");
				break;
			}
			}
			break;
		}
		case 0: {
			uint32_t reg_base = (dwords[i]) & 0x7fff;
			for (uint32_t j = 0; j < cnt; j++) {
				uint32_t reg = reg_base + j;
				uint32_t value = dwords[i + 1 + j];

				if (j == 0)
					printf("\twr(cs, ");
				else
					printf("\t    /* ");

				const char *name = lookup_register(reg);
				if (name)
					printf("%s", name);
				else
					printf("0x%04x", reg);

				if (j == 0)
					printf(", ");
				else
					printf(" */ ");

				uint64_t offset = 0;
				for (uint32_t k = 0; k < ARRAY_LENGTH(address_regs); k++) {
					uint32_t size = regs.value[address_regs[k].len];
					if (size == 0)
						continue;
					if (address_regs[k].base == reg) {
						offset = regs.value[reg] & ~0xf;
						printf("stream_0x%lx.offset", offset);
						break;
					}

				}
				if (offset == 0)
					printf("0x%08x", value);

				if (j < cnt - 1)
					printf(",\n");
				else
					printf(");\n");

			}
			break;
		}
		}
		i += cnt + 1;
	}

	printf("\n\tstream_0x%lx = finish_stream(cs);\n}\n\n", start);
}

int main(int argc, char *argv[])
{
	fail_if(argc < 2, "usage: rd2replay FILE");;
	const char *filename = argv[1];
	FILE *fp = fopen(filename, "rb");
	fail_if(fp == NULL, "failed to open %s: %m", argv[1]);

	uint64_t gpuaddr_start;
	uint32_t gpuaddr_size;

	printf("#define GEN 4\n");
	printf("#include \"replay-helper.h\"\n\n");

	struct cmdstream {
		uint64_t addr;
		uint32_t length;
	} cmd_streams[16];
	int count = 0;

	while (true) {
		uint32_t header[2];
		size_t n = fread(header, sizeof(header), 1, fp);
		if (feof(fp))
			break;
		fail_if(n != 1, "unexpected header size");
		uint32_t type = header[0];
		uint32_t size = header[1];

		switch (type) {
		case RD_GPUADDR: {
			fail_if(size != 8, "unexpected RD_GPUADDR size: %d", size);
			uint32_t block[2];
			xfread(block, sizeof(block), 1, fp);
			gpuaddr_start = block[0]; // | ((uint64_t) block[2] << 32);
			gpuaddr_size = block[1];
			break;
		}
		case RD_BUFFER_CONTENTS: {
			void *buffer = xmalloc(size);
			xfread(buffer, 1, size, fp);

			fail_if(num_bos == ARRAY_LENGTH(replay_bos), "out of bos");
			struct bo *bo = &replay_bos[num_bos++];
			bo->iova = gpuaddr_start;
			bo->size = gpuaddr_size;
			bo->contents = buffer;
			break;
		}
		case RD_CMDSTREAM_ADDR: {
			fail_if(size != 8, "unexpected RD_CMDSTREAM_ADDR size: %d", size);
			uint32_t block[2];
			xfread(block, sizeof(block), 1, fp);

			uint64_t cmd_start = block[0]; // | ((uint64_t) block[2] << 32);
			uint32_t cmd_length = block[1];

			cmd_streams[count].addr = cmd_start;
			cmd_streams[count].length = cmd_length;
			if (cmd_length > 0)
				dump_cmdstream(cmd_start, cmd_length, "toplevel invocation %d", count);
			count++;
			break;
		}
		case RD_CMD: {
			static char buf[65536];
			fail_if(size + 1 > ARRAY_LENGTH(buf), "RD_CMD section too big");
			xfread(buf, 1, size, fp);
			buf[size] = '\0';
			printf("/* RD_CMD: %s */\n\n", buf);
			break;
		}
		case RD_TEST: {
			char buf[256];
			fail_if(size + 1 > ARRAY_LENGTH(buf), "RD_TEST section too big");
			xfread(buf, 1, size, fp);
			buf[size] = '\0';
			printf("/* RD_TEST: %s */\n\n", buf);
			break;
		}
		case RD_GPU_ID: {
			fail_if(size != 4, "unexpected RD_GPU_ID size: %d", size);
			uint32_t gpu_id;
			xfread(&gpu_id, sizeof(gpu_id), 1, fp);
			printf("/* RD_GPU_ID: %d */\n\n", gpu_id);
			break;
		}
                case -1:
			fail_if(size != -1, "unexpected skip size: %d", size);
			break;
		default: {
			printf("// ------------- skipping %d bytes for unhandled block %d\n", size, type);
			void *buffer = xmalloc(size);
			xfread(buffer, 1, size, fp);
			free(buffer);
			break;
		}
		}
	}

	printf("int main(int argc, char *argv[])\n{\n");

	printf("\tstruct allocator cs;\n"
	       "\tinit_allocator(&cs, 64 << 20);\n\n");

	for (uint32_t i = 0; i < num_streams; i++)
		printf("\tbuild_stream_0x%lx(&cs);\n", streams[i]);
	printf("\n");

	for (uint32_t i = 0; i < count; i++) {
		if (cmd_streams[i].length == 0) {
			printf("\t/* empty cmd stream 0x%lx */\n\n", cmd_streams[i].addr);
			continue;
		} else {
			fail_if(!did_stream(cmd_streams[i].addr), "uninitialized stream");
			printf("\tsubmit_stream(&cs, stream_0x%lx);\n\n",
			       cmd_streams[i].addr);
		}
	}

	printf("\treturn 0;\n}\n");

	return 0;
}
