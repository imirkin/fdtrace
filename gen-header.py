#!/usr/bin/python3

import xml.parsers.expat
import sys

class Register(object):
    def __init__(self, name, offset):
        self.name = name
        self.offset = offset

class Field(object):
    def __init__(self, name, low, high, type):
        self.name = name
        self.low = low
        self.high = high
        self.type = type

class Enum(object):
    def __init__(self, name):
        self.name = name
        self.values = {}

class Parser(object):
    def __init__(self):
        self.current_register = None
        self.current_array = None
        self.current_domain = None
        self.reg_name = None
        self.registers = [ Register("", 0) ]
        self.fields = []
        self.enums = {}
        self.current_enum = None

    def start_element(self, name, attrs):
        if name == "import":
            print("/* import %s */" % (attrs["file"]))
        elif name == "enum":
            e = Enum(attrs["name"]);
            self.enums[e.name] = e
            self.current_enum = e
        elif name == "value":
            if "variants" in attrs and not GEN in attrs["variants"].split(","):
                return
            if "value" in attrs:
                self.current_enum.values[attrs["name"]] = int(attrs["value"], 0)
        elif name == "reg32":
            if not self.current_domain == GEN:
                return

            self.current_register = Register(attrs["name"], int(attrs["offset"], 0))
        elif name == "array":
            self.current_array = attrs["name"];
            self.array_offset = int(attrs["offset"], 0)
            self.array_stride = int(attrs["stride"], 0)
            self.array_length = int(attrs["length"], 0)
        elif name == "domain":
            self.current_domain = attrs["name"];
        elif name == "bitfield":
            if "pos" in attrs:
                high = low = attrs["pos"]
            else:
                high = attrs["high"]
                low = attrs["low"]

            if "type" in attrs:
                type = attrs["type"]
            else:
                type = "uint"

            if type == "ufixed":
                radix = attrs["radix"]

            b = Field(attrs["name"], low, high, type)
            self.fields.append(b)

    def end_element(self, name):
        if name == "enum":
            self.current_enum = None
        elif name == "array":
            self.current_array = None;
        elif name == "domain":
            self.current_domain = None;
        elif name == "reg32":
            if not self.current_register:
                return
            if self.current_array:
                for i in range(self.array_length):
                    r = Register(self.current_array + str(i) + "_" + self.current_register.name,
                                 self.array_offset + self.current_register.offset + i * self.array_stride)
                    self.registers.append(r);
            else:
                self.registers.append(self.current_register)

            self.current_register = None

    def parse(self, filename):
        file = open(filename, "rb")
        self.parser = xml.parsers.expat.ParserCreate()
        self.parser.StartElementHandler = self.start_element
        self.parser.EndElementHandler = self.end_element
        self.parser.ParseFile(file)
        file.close()

    def dump_register_names(self):
        print("enum registers {")
        for r in self.registers:
            if not r.name == "":
                print("\t%-32s = 0x%08x," % (r.name, r.offset))
        print("};\n")

        print("\nstatic const char register_names[] =")
        i = 0
        for r in self.registers:
            print("\t\"%s\\0\"" % r.name)
            r.pool_index = i;
            i = i + len(r.name) + 1
        print("\t;\n")

        i = 0
        print("static const struct { uint32_t pool_index; uint32_t offset; } registers[] = {")
        for r in self.registers:
            print("\t{ %8d, %8d },\t/* %s */" % (r.pool_index, r.offset, r.name))
            i = i + 1
        print("};\n")

        print("/* total regs: %d */" % len(self.registers))

        size = 16
        while size < 2 * len(self.registers):
            size = size * 2

        hash = [0] * size
        prime = 2657
        step = 181
        collisions = [0] * 20
        for i, r in enumerate(self.registers):
            h = (r.offset * prime) & (0xffffffff)
            h = h & (size - 1)
            c = 0
            while not hash[h] == 0:
                h = (h + step) & (size - 1)
                c = c + 1
            hash[h] = i
            if c > 19:
                c = 19
            collisions[c] = collisions[c] + 1

        print("static const uint16_t register_hash[] = {")
        base = 0
        while base < size:
            print("\t", end="")
            for i in range(16):
                print("%5d" % hash[base + i], end=", ")
            print("")
            base = base + 16
        print("};\n")

        print("""
const char *
lookup_register(uint32_t offset)
{
        const uint32_t prime = %d;
        const uint32_t size = %d;
        const uint32_t step = %d;

        uint32_t h = (offset * prime) & (0xffffffffu);
        h = h & (size - 1);
        while (register_hash[h] != 0 && registers[register_hash[h]].offset != offset) {
                h = (h + step) & (size - 1);
        }

        if (register_hash[h] == 0)
                return NULL;

        return register_names + registers[register_hash[h]].pool_index;
}
        """ % (prime, size, step))

        print(("/* hash table stats:\n" +
               " *   size:    %d\n" +
               " *   entries: %d\n" +
               " *   prime: %d\n" +
               " *   collisions:") % (size, len(self.registers), prime))
        for i, c in enumerate(collisions):
            print(" *      %2d hits: %5d" % (i, c))
        print(" */\n")

    def dump_enum(self, e, lookup_function):
        print("enum %s {" % e.name)
        for name, value in e.values.items():
            print("\t%-32s = 0x%08x," % (name, value))
        print("};\n")

        if lookup_function:
            print("static const char %s_names[] = " % e.name)
            offsets = []
            print("\t\"\\0\"")
            current_offset = 1
            for name in e.values:
                offsets.append(current_offset);
                current_offset = current_offset + len(name) + 1
                print("\t\"%s\\0\"" % name)
            print("\t;\n")

            print("static const uint16_t %s_offsets[] = {" % e.name)
            for i, name in enumerate(e.values):
                print("\t[%s] = %d," % (name, offsets[i]))
            print("};\n")

            print("""const char *
%s(uint32_t opcode)
{
        if (opcode < ARRAY_LENGTH(%s_offsets) &&
            %s_offsets[opcode] != 0)
                return %s_names + %s_offsets[opcode];
        else
                return NULL;
}
                """ % (lookup_function, e.name, e.name, e.name, e.name));

def main():
    global GEN
    GEN = sys.argv[3].upper()

    p = Parser()
    p.parse(sys.argv[1])
    p.parse(sys.argv[2])

    assert(GEN == "A4XX" or GEN == "A6XX", "unexpected GEN")

    p.dump_register_names()

    packets = p.enums["adreno_pm4_type3_packets"]
    p.dump_enum(packets, "lookup_pkt7_name")

    lower_gen = GEN.lower()
    enums = [ "adreno_pm4_packet_type", lower_gen + "_state_block", lower_gen + "_state_src", lower_gen + "_state_type" ]
    for e in enums:
        p.dump_enum(p.enums[e], None);

if __name__ == '__main__':
    main()
