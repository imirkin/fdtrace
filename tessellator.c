#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include <libpng16/png.h>

#include <EGL/egl.h>
#include <GLES3/gl32.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

static void
fail_if(int cond, const char *fmt, ...)
{
	va_list va;

	if (cond) {
		va_start(va, fmt);
		vfprintf(stderr, fmt, va);
		fprintf(stderr, "\n");
		va_end(va);
		exit(EXIT_FAILURE);
	}
}

static void *
xmalloc(size_t size)
{
	void *p = malloc(size);

	fail_if(p == NULL, "malloc failed");

	return p;
}

static int get_env_int(const char *var, int default_value)
{
	char *s = getenv(var);
	int value;
	if (s == NULL)
		value = default_value;
	else
		value = atoi(s);

	printf("%s=%d\n", var, value);

	return value;
}

static int num_verts, num_attrs, num_tris;

GLuint compile_shader(GLenum type, const GLchar *src)
{
	const char *stage;

	switch (type) {
	case GL_VERTEX_SHADER:
		stage = "vertex";
		break;
	case GL_TESS_CONTROL_SHADER:
		stage = "tess_control";
		break;
	case GL_TESS_EVALUATION_SHADER:
		stage = "tess_evaluation";
		break;
	case GL_FRAGMENT_SHADER:
		stage = "fragment";
		break;
	}

	if (get_env_int("DEBUG", 0))
		printf("--- %s shader: -----------\n%s", stage, src);

	GLuint shader = glCreateShader(type);
	glShaderSource(shader, 1, &src, NULL);
	glCompileShader(shader);

	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	char msg[4096];
	glGetShaderInfoLog(shader, sizeof(msg), NULL, msg);
	fail_if(!status, "failed to compile shader: %s", msg);

	return shader;
}

struct string {
	char data[4096];
	int len;
};

static void string_reset(struct string *s)
{
	s->len = 0;
	s->data[0] = '\0';
}

static void string_printf(struct string *s, const char *fmt, ...)
{
	va_list va;

	va_start(va, fmt);
	s->len += vsnprintf(s->data + s->len, sizeof(s->data) - s->len, fmt, va);
	va_end(va);
}

static GLuint solid_shader_create()
{
	struct string src;

	GLuint program = glCreateProgram();
	fail_if(!program, "failed to create program");

	string_reset(&src);
	string_printf(&src, "#version 320 es\n"
		      "in vec4 vPosition;\n"
		      "in vec4 vColor;\n");
	for (int i = 0; i < num_attrs; i++)
		string_printf(&src, "out vec4 vs_out%d;\n", i);

	string_printf(&src, "void main() {\n"
		      "    gl_Position = vPosition;\n"
		      "    vs_out0 = vColor;\n");
	for (int i = 1; i < num_attrs; i++)
		string_printf(&src, "    vs_out%d = vs_out%d + vPosition;\n", i, i - 1);
	string_printf(&src, "}\n");

	GLuint vert_shader = compile_shader(GL_VERTEX_SHADER, src.data);

	string_reset(&src);
	string_printf(&src, "#version 320 es\n"
		      "layout (vertices=%d) out;\n", num_verts);
	for (int i = 0; i < num_attrs; i++)
		string_printf(&src, "in mediump vec4 vs_out%d[];\n", i);

	string_printf(&src, "out mediump vec4 tcs_out_color[];\n"
		      "void main() {\n"
		      "    tcs_out_color[gl_InvocationID] = vs_out0[gl_InvocationID]");
	for(int i = 1; i < num_attrs; i++)
		string_printf(&src, " + vs_out%d[gl_InvocationID]", i);

	string_printf(&src, ";\n"
		      "    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;\n"
		      "    gl_TessLevelInner[0] = 2.0;\n"
		      "    gl_TessLevelInner[1] = 2.0;\n"
		      "    gl_TessLevelOuter[0] = 2.0;\n"
		      "    gl_TessLevelOuter[1] = 2.0;\n"
		      "    gl_TessLevelOuter[2] = 2.0;\n"
		      "    gl_TessLevelOuter[3] = 2.0;\n"
		      "}\n");

	GLuint tcs_shader = compile_shader(GL_TESS_CONTROL_SHADER, src.data);

	string_reset(&src);
	string_printf(&src, "#version 320 es\n"
		      "layout (triangles) in;\n"
		      "in mediump vec4 tcs_out_color[];\n"
		      "out mediump vec4 tes_out_color;\n"
		      "void main() {\n"
		      "    tes_out_color = gl_TessCoord[0] * tcs_out_color[0] + gl_TessCoord[1] * tcs_out_color[1] + gl_TessCoord[2] * tcs_out_color[2];\n"
		      "    gl_Position = gl_TessCoord[0] * gl_in[0].gl_Position + gl_TessCoord[1] * gl_in[1].gl_Position + gl_TessCoord[2] * gl_in[2].gl_Position;\n"
		      "}\n");

	GLuint tes_shader = compile_shader(GL_TESS_EVALUATION_SHADER, src.data);

	string_reset(&src);
	string_printf(&src, "#version 320 es\n"
		      "precision mediump float;\n"
		      "layout(location = 0) out mediump vec4 color;\n"
		      "in vec4 tes_out_color;\n"
		      "void main() {\n"
		      "    color = tes_out_color;\n"
		      "}\n");

	GLuint frag_shader = compile_shader(GL_FRAGMENT_SHADER, src.data);

	glAttachShader(program, vert_shader);
	glAttachShader(program, tcs_shader);
	glAttachShader(program, tes_shader);
	glAttachShader(program, frag_shader);

	glBindAttribLocation(program, 0, "vPosition");
	glBindAttribLocation(program, 1, "vColor");

	glLinkProgram(program);
	glDetachShader(program, vert_shader);
	glDetachShader(program, tcs_shader);
	glDetachShader(program, tes_shader);
	glDetachShader(program, frag_shader);
	glDeleteShader(vert_shader);
	glDeleteShader(tcs_shader);
	glDeleteShader(tes_shader);
	glDeleteShader(frag_shader);

	GLint status;
	glGetProgramiv(program, GL_LINK_STATUS, &status);

	char msg[1024];
	glGetProgramInfoLog(program, sizeof(msg), NULL, msg);
	fail_if(!status, "failed to link program: %s", msg);

	return program;
}

int main(int argc, char **argv)
{
	num_verts = get_env_int("NUM_VERTS", 3);
	num_attrs = get_env_int("NUM_ATTRS", 1);
	num_tris = get_env_int("NUM_TRIS", 10);

	static const char device_filename[] = "/dev/dri/renderD128";
	int fd = open(device_filename, O_RDWR);
	fail_if(fd < 0, "failed to open card %s", device_filename);

	EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	fail_if(display == EGL_NO_DISPLAY, "failed to get egl display");

	EGLBoolean success;
	success = eglInitialize(display, NULL /* ignore version */, NULL /* ignore version */);
	fail_if(!success, "failed to initialize egl\n");

	// Get any EGLConfig. We need one to create a context, but it isn't
	// used to create any surfaces.
	static const EGLint config_attribs[] = {
		EGL_SURFACE_TYPE, EGL_DONT_CARE, EGL_RENDERABLE_TYPE,
		EGL_OPENGL_ES2_BIT, EGL_NONE
	};
	EGLConfig egl_config;
	EGLint num_configs;
	success = eglChooseConfig(display, config_attribs, &egl_config, 1,
				&num_configs /* unused but can't be null */);
	fail_if(!success, "eglChooseConfig() failed with errors");

	success = eglBindAPI(EGL_OPENGL_ES_API);
	fail_if(!success, "failed to bind OpenGL ES");

	static const EGLint context_attribs[] = {
		EGL_CONTEXT_CLIENT_VERSION, 3, EGL_NONE
	};

	EGLContext ctx = eglCreateContext(display, egl_config,
					  EGL_NO_CONTEXT /* no shared context */,
					  context_attribs);
	fail_if (ctx == EGL_NO_CONTEXT,
		 "failed to create OpenGL ES Context");

	success = eglMakeCurrent(display, EGL_NO_SURFACE /* no default draw surface */,
				EGL_NO_SURFACE /* no default draw read */, ctx);
	fail_if(!success, "failed to make the OpenGL ES Context current");

	const int width = 1024, height = 1024;
	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);

	GLuint fb;
	glGenFramebuffers(1, &fb);
	glBindFramebuffer(GL_FRAMEBUFFER, fb);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

	fail_if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE,
		"failed framebuffer check for created target buffer");

	glEnable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	glStencilFunc(GL_ALWAYS, 7, 0xffu);

	GLuint program = solid_shader_create();
	fail_if(!program, "failed to create solid shader");

	static const GLfloat const_verts[] = {
		0.0f, -0.9f, 0.0f,
		-0.9f, 0.9f, 0.0f,
		0.9f, 0.9f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
	};
	GLfloat verts[256];
	GLfloat colors[] = {
		1.0f, 0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
	};

	glBindFramebuffer(GL_FRAMEBUFFER, fb);
	glViewport(0, 0, width, height);

	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(program);

	glPatchParameteri(GL_PATCH_VERTICES, num_verts);

	fail_if(ARRAY_SIZE(const_verts) / 3 < num_verts, "too many vertices");

	float x = -0.5;
	for (int i = 0; i < num_tris; i++) {
		for (uint32_t i = 0; i < num_verts * 3; i += 3) {
			verts[i + 0] = const_verts[i+ 0] + x;
			verts[i + 1] = const_verts[i+ 1];
			verts[i + 2] = const_verts[i+ 2];
		}
		x += 0.1f;

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, verts);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, colors);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		glDrawArrays(GL_PATCHES, 0, num_verts);
	}

	char *buffer;
	uint32_t stride = width * 4;
	buffer = xmalloc(stride * height);

	glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

	fail_if(glGetError() != GL_NO_ERROR, "glReadPixels failed\n");

	png_image pi = {
		.version = PNG_IMAGE_VERSION,
		.width = width,
		.height = height,
		.format = PNG_FORMAT_RGBA
	};

	FILE *f = fopen("tessellator.png", "wb");
	fail_if(f == NULL, "failed to open output file");

	fail_if(!png_image_write_to_stdio(&pi, f, 0, buffer, stride, NULL),
		"failed to write output");

	fclose(f);

	return 0;
}
