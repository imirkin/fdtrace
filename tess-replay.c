#include <libpng16/png.h>

#include "replay-helper.h"

/* RD_CMD: tracing: /tmp/tessellator (pid 2214)
output to file /tmp/fdtrace.rd
 */

/* RD_GPU_ID: 630 */

static struct stream stream_0x1025000;
static void
build_stream_0x1025000(struct allocator *cs)
{
	/* CP_MEM_WRITE from stream 0x1027000, 16 bytes */

	stream_0x1025000 = allocate_block(cs, NULL, 16, 16);
}

static struct stream stream_0x1025008;
static void
build_stream_0x1025008(struct allocator *cs)
{
	/* CP_MEM_WRITE from stream 0x1027000, 16 bytes */

	stream_0x1025008 = allocate_block(cs, NULL, 16, 16);
}

static struct stream stream_0x1026000;
static void
build_stream_0x1026000(struct allocator *cs)
{
	/* CP_MEM_TO_REG from stream 0x1027000, 128 bytes */

	static const uint32_t contents[] = {
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	};

	stream_0x1026000 = allocate_block(cs, contents, sizeof(contents), 16);
}

static struct stream stream_0x1027000;
static void
build_stream_0x1027000(struct allocator *cs)
{
	/* CP_UNK_A6XX_55 from stream 0x1804000, 92 dwords */

	begin_stream(cs, 4);

	cp(cs, CP_MEM_WRITE, stream_0x1025000.offset, stream_0x1025000.offset >> 32, 0x1);
	cp(cs, CP_MEM_WRITE, stream_0x1025008.offset, stream_0x1025008.offset >> 32, 0x1);
	wr(cs, RB_CCU_CNTL, 0x7c400004);
	wr(cs, RB_UNKNOWN_8E04, 0x00100000);
	wr(cs, SP_UNKNOWN_AE04, 0x00000008);
	wr(cs, SP_UNKNOWN_AE00, 0x00000000);
	wr(cs, SP_UNKNOWN_AE0F, 0x0000003f);
	wr(cs, SP_UNKNOWN_B605, 0x00000044);
	wr(cs, SP_UNKNOWN_B600, 0x00100000);
	wr(cs, HLSQ_UNKNOWN_BE00, 0x00000080,
	    /* HLSQ_UNKNOWN_BE01 */ 0x00000000);
	wr(cs, VPC_UNKNOWN_9600, 0x00000000);
	wr(cs, GRAS_UNKNOWN_8600, 0x00000880);
	wr(cs, HLSQ_UNKNOWN_BE04, 0x00080000);
	wr(cs, SP_UNKNOWN_AE03, 0x00001430);
	wr(cs, UCHE_UNKNOWN_0E12, 0x03200000);
	wr(cs, RB_UNKNOWN_8E01, 0x00000001);
	wr(cs, SP_UNKNOWN_AB00, 0x00000005);
	wr(cs, VFD_UNKNOWN_A009, 0x00000001);
	wr(cs, RB_UNKNOWN_8811, 0x00000010);
	wr(cs, PC_MODE_CNTL, 0x0000001f);
	wr(cs, PC_RESTART_INDEX, 0xffffffff);
	wr(cs, GRAS_SU_POINT_MINMAX, 0x3ff00010,
	    /* GRAS_SU_POINT_SIZE */ 0x00000008);
	wr(cs, GRAS_UNKNOWN_8099, 0x00000000);
	wr(cs, GRAS_UNKNOWN_80AF, 0x00000000);
	wr(cs, VPC_UNKNOWN_9210, 0x00000000,
	    /* VPC_UNKNOWN_9211 */ 0x00000000);
	wr(cs, VPC_UNKNOWN_9602, 0x00000000);
	wr(cs, PC_UNKNOWN_9981, 0x00000003);
	wr(cs, PC_UNKNOWN_9E72, 0x00000000);
	wr(cs, VPC_UNKNOWN_9108, 0x00000003);
	wr(cs, SP_TP_UNKNOWN_B309, 0x000000b2);
	wr(cs, GRAS_UNKNOWN_80A5, 0x00000000);
	wr(cs, GRAS_UNKNOWN_80A6, 0x00000000);
	wr(cs, RB_UNKNOWN_8805, 0x00000000);
	wr(cs, RB_UNKNOWN_8806, 0x00000000);
	wr(cs, RB_UNKNOWN_8878, 0x00000000,
	    /* RB_UNKNOWN_8879 */ 0x00000000);
	wr(cs, HLSQ_CONTROL_5_REG, 0x000000fc);
	cp(cs, CP_MEM_TO_REG, 0xc38 | ((stream_0x1026000.size / 4) << 19),
	   stream_0x1026000.offset, stream_0x1026000.offset >> 32);
	wr(cs, UCHE_CLIENT_PF, 0x00000004);
	wr(cs, VFD_MODE_CNTL, 0x00000000);
	wr(cs, PC_UNKNOWN_9805, 0x00000001);
	wr(cs, SP_UNKNOWN_A0F8, 0x00000001);

	stream_0x1027000 = finish_stream(cs);
}

static struct stream stream_0x1024000;
static void
build_stream_0x1024000(struct allocator *cs)
{
	/* CP_EVENT_WRITE from stream 0x1804000, 4 bytes */

	stream_0x1024000 = allocate_block(cs, NULL, 4, 4);
}

static struct stream stream_0x1040000;
static void
build_stream_0x1040000(struct allocator *cs)
{
	/* RB_2D_DST_LO from stream 0x1550040, 5242880 bytes */

	stream_0x1040000 = allocate_block(cs, NULL, 5242880, 64);
}

static struct stream stream_0x1550040;
static void
build_stream_0x1550040(struct allocator *cs)
{
	/* CP_INDIRECT_BUFFER from stream 0x1804000, 95 dwords */

	begin_stream(cs, 4);

	cp(cs, CP_EVENT_WRITE, 0x1d, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0x2);
	cp(cs, CP_EVENT_WRITE, 0x1c, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0x3);
	cp(cs, CP_EVENT_WRITE, 0x19);
	cp(cs, CP_EVENT_WRITE, 0x18);
	cp(cs, CP_SET_MARKER, 0x0000000c);
	wr(cs, RB_UNKNOWN_8C01, 0x00000000);
	wr(cs, SP_PS_2D_SRC_INFO, 0x00000000,
	    /* SP_PS_2D_SRC_SIZE */ 0x00000000,
	    /* SP_PS_2D_SRC_LO */ 0x00000000,
	    /* SP_PS_2D_SRC_HI */ 0x00000000,
	    /* SP_PS_2D_SRC_PITCH */ 0x00000000,
	    /* 0xb4c5 */ 0x00000000,
	    /* 0xb4c6 */ 0x00000000,
	    /* 0xb4c7 */ 0x00000000,
	    /* 0xb4c8 */ 0x00000000,
	    /* 0xb4c9 */ 0x00000000,
	    /* SP_PS_2D_SRC_FLAGS_LO */ 0x00000000,
	    /* SP_PS_2D_SRC_FLAGS_HI */ 0x00000000,
	    /* 0xb4cc */ 0x00000000);
	wr(cs, SP_2D_SRC_FORMAT, 0x0000f181);
	wr(cs, GRAS_2D_BLIT_CNTL, 0x00f43080);
	wr(cs, RB_2D_BLIT_CNTL, 0x00f43080);
	cp(cs, CP_EVENT_WRITE, 0x1d, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0x0);
	cp(cs, CP_EVENT_WRITE, 0x19);
	wr(cs, RB_2D_SRC_SOLID_C0, 0x00000000,
	    /* RB_2D_SRC_SOLID_C1 */ 0x00000000,
	    /* RB_2D_SRC_SOLID_C2 */ 0x000000ff,
	    /* RB_2D_SRC_SOLID_C3 */ 0x00000000);
	wr(cs, RB_2D_DST_INFO, 0x00000030,
	    /* RB_2D_DST_LO */ stream_0x1040000.offset,
	    /* RB_2D_DST_HI */ stream_0x1040000.offset >> 32,
	    /* RB_2D_DST_SIZE */ DIV_ROUND_UP(stream_0x1040000.size, 1),
	    /* 0x8c1b */ 0x00000000,
	    /* 0x8c1c */ 0x00000000,
	    /* 0x8c1d */ 0x00000000,
	    /* 0x8c1e */ 0x00000000,
	    /* 0x8c1f */ 0x00000000);
	wr(cs, GRAS_2D_SRC_TL_X, 0x00000000,
	    /* GRAS_2D_SRC_BR_X */ 0x00000000,
	    /* GRAS_2D_SRC_TL_Y */ 0x00000000,
	    /* GRAS_2D_SRC_BR_Y */ 0x00000000);
	wr(cs, GRAS_2D_DST_TL, 0x00000000,
	    /* GRAS_2D_DST_BR */ 0x03ff03ff);
	cp(cs, CP_EVENT_WRITE, 0x3f);
	cp(cs, CP_WAIT_FOR_IDLE);
	wr(cs, RB_UNKNOWN_8E04, 0x01000000);
	cp(cs, CP_BLIT, 0x00000003);
	cp(cs, CP_WAIT_FOR_IDLE);
	wr(cs, RB_UNKNOWN_8E04, 0x00000000);
	cp(cs, CP_EVENT_WRITE, 0x1d, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0x4);
	cp(cs, CP_EVENT_WRITE, 0x1c, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0x5);
	cp(cs, CP_EVENT_WRITE, 0x4, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0x6);
	cp(cs, CP_EVENT_WRITE, 0x31);

	stream_0x1550040 = finish_stream(cs);
}

static struct stream stream_0x1804000;
static void
build_stream_0x1804000(struct allocator *cs)
{
	/* toplevel invocation 0, 200 dwords */

	begin_stream(cs, 4);

	cp(cs, CP_UNK_A6XX_55, stream_0x1027000.offset, stream_0x1027000.offset >> 32, 0x100000 | stream_0x1027000.size / 4);
	cp(cs, CP_EVENT_WRITE, 0x31);
	cp(cs, CP_WAIT_FOR_IDLE);
	cp(cs, CP_WAIT_FOR_IDLE);
	wr(cs, RB_CCU_CNTL, 0x7c400004);
	wr(cs, RB_UNKNOWN_8E04, 0x00100000);
	wr(cs, SP_UNKNOWN_AE04, 0x00000008);
	wr(cs, SP_UNKNOWN_AE00, 0x00000000);
	wr(cs, SP_UNKNOWN_AE0F, 0x0000003f);
	wr(cs, SP_UNKNOWN_B605, 0x00000044);
	wr(cs, SP_UNKNOWN_B600, 0x00100000);
	wr(cs, HLSQ_UNKNOWN_BE00, 0x00000080,
	    /* HLSQ_UNKNOWN_BE01 */ 0x00000000);
	wr(cs, VPC_UNKNOWN_9600, 0x00000000);
	wr(cs, GRAS_UNKNOWN_8600, 0x00000880);
	wr(cs, HLSQ_UNKNOWN_BE04, 0x00080000);
	wr(cs, SP_UNKNOWN_AE03, 0x00001430);
	wr(cs, UCHE_UNKNOWN_0E12, 0x03200000);
	wr(cs, RB_UNKNOWN_8E01, 0x00000001);
	wr(cs, SP_UNKNOWN_AB00, 0x00000005);
	wr(cs, VFD_UNKNOWN_A009, 0x00000001);
	wr(cs, RB_UNKNOWN_8811, 0x00000010);
	wr(cs, PC_MODE_CNTL, 0x0000001f);
	wr(cs, PC_RESTART_INDEX, 0xffffffff);
	wr(cs, GRAS_SU_POINT_MINMAX, 0x3ff00010,
	    /* GRAS_SU_POINT_SIZE */ 0x00000008);
	wr(cs, GRAS_UNKNOWN_8099, 0x00000000);
	wr(cs, GRAS_UNKNOWN_80AF, 0x00000000);
	wr(cs, VPC_UNKNOWN_9210, 0x00000000,
	    /* VPC_UNKNOWN_9211 */ 0x00000000);
	wr(cs, VPC_UNKNOWN_9602, 0x00000000);
	wr(cs, PC_UNKNOWN_9981, 0x00000003);
	wr(cs, PC_UNKNOWN_9E72, 0x00000000);
	wr(cs, VPC_UNKNOWN_9108, 0x00000003);
	wr(cs, SP_TP_UNKNOWN_B309, 0x000000b2);
	wr(cs, GRAS_UNKNOWN_80A5, 0x00000000);
	wr(cs, GRAS_UNKNOWN_80A6, 0x00000000);
	wr(cs, RB_UNKNOWN_8805, 0x00000000);
	wr(cs, RB_UNKNOWN_8806, 0x00000000);
	wr(cs, RB_UNKNOWN_8878, 0x00000000,
	    /* RB_UNKNOWN_8879 */ 0x00000000);
	wr(cs, HLSQ_CONTROL_5_REG, 0x000000fc);
	cp(cs, CP_MEM_TO_REG, 0xc38 | ((stream_0x1026000.size / 4) << 19),
	   stream_0x1026000.offset, stream_0x1026000.offset >> 32);
	wr(cs, UCHE_CLIENT_PF, 0x00000004);
	cp(cs, CP_SET_DRAW_STATE,
	   0x00040000, 0x00000000, 0x00000000);
	wr(cs, GRAS_LRZ_CNTL, 0x00000000);
	cp(cs, CP_EVENT_WRITE, 0x26);
	cp(cs, CP_EVENT_WRITE, 0x1d, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0x7);
	cp(cs, CP_EVENT_WRITE, 0x1c, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0x8);
	cp(cs, CP_EVENT_WRITE, 0x19);
	cp(cs, CP_EVENT_WRITE, 0x18);
	cp(cs, CP_SKIP_IB2_ENABLE_GLOBAL, 0x00000000);
	cp(cs, CP_SET_DRAW_STATE,
	   0x00040000, 0x00000000, 0x00000000);
	wr(cs, RB_UNKNOWN_88F0, 0x00000000);
	cp(cs, CP_WAIT_FOR_IDLE);
	wr(cs, RB_CCU_CNTL, 0x10000000);
	wr(cs, RB_BIN_CONTROL, 0x00c00000);
	wr(cs, GRAS_BIN_CONTROL, 0x00c00000);
	wr(cs, VFD_MODE_CNTL, 0x00000000);
	wr(cs, PC_UNKNOWN_9805, 0x00000001);
	wr(cs, SP_UNKNOWN_A0F8, 0x00000001);
	cp(cs, CP_SKIP_IB2_ENABLE_GLOBAL, 0x00000000);
	cp(cs, CP_SET_MARKER, 0x00000001);
	wr(cs, GRAS_SC_WINDOW_SCISSOR_TL, 0x00000000,
	    /* GRAS_SC_WINDOW_SCISSOR_BR */ 0x03ff03ff);
	wr(cs, GRAS_RESOLVE_CNTL_1, 0x00000000,
	    /* GRAS_RESOLVE_CNTL_2 */ 0x03ff03ff);
	wr(cs, RB_WINDOW_OFFSET, 0x00000000);
	wr(cs, SP_TP_WINDOW_OFFSET, 0x00000000);
	wr(cs, SP_WINDOW_OFFSET, 0x00000000);
	wr(cs, RB_WINDOW_OFFSET2, 0x00000000);
	wr(cs, RB_BIN_CONTROL2, 0x00000000);
	wr(cs, RB_BIN_CONTROL, 0x00c00000);
	wr(cs, GRAS_BIN_CONTROL, 0x00c00000);
	wr(cs, VPC_SO_OVERRIDE, 0x00000001);
	cp(cs, CP_SET_VISIBILITY_OVERRIDE, 0x00000001);
	cp(cs, CP_SET_MODE, 0x00000000);
	wr(cs, RB_UNKNOWN_8804, 0x00000000);
	wr(cs, SP_TP_UNKNOWN_B304, 0x00000000);
	wr(cs, GRAS_UNKNOWN_80A4, 0x00000000);
	ib(cs, stream_0x1550040);
	cp(cs, CP_SET_DRAW_STATE,
	   0x00040000, 0x00000000, 0x00000000);
	cp(cs, CP_SKIP_IB2_ENABLE_LOCAL, 0x00000000);
	cp(cs, CP_SKIP_IB2_ENABLE_GLOBAL, 0x00000000);
	wr(cs, GRAS_LRZ_CNTL, 0x00000009);
	cp(cs, CP_EVENT_WRITE, 0x26);
	cp(cs, CP_EVENT_WRITE, 0x1d, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0x9);
	cp(cs, CP_EVENT_WRITE, 0x4, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0xa);
	cp(cs, CP_EVENT_WRITE, 0x31);
	cp(cs, CP_UNK_A6XX_14, 0x0, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0xa);
	wr(cs, RB_UNKNOWN_88F0, 0x00000000);

	stream_0x1804000 = finish_stream(cs);
}

static struct stream stream_0x1590030;
static void
build_stream_0x1590030(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 0 from stream 0x1800000, 21 dwords */

	begin_stream(cs, 4);

	wr(cs, HLSQ_UPDATE_CNTL, 0x000000ff);
	wr(cs, HLSQ_VS_CNTL, 0x00000104,
	    /* HLSQ_HS_CNTL */ 0x00000104,
	    /* HLSQ_DS_CNTL */ 0x00000103,
	    /* HLSQ_GS_CNTL */ 0x00000000);
	wr(cs, HLSQ_FS_CNTL, 0x00000101);
	wr(cs, SP_VS_CONFIG, 0x00000100);
	wr(cs, SP_FS_CONFIG, 0x00000100);
	wr(cs, SP_HS_CONFIG, 0x00000100);
	wr(cs, SP_DS_CONFIG, 0x00000100);
	wr(cs, SP_GS_CONFIG, 0x00000000);
	wr(cs, SP_IBO_COUNT, 0x00000000);

	stream_0x1590030 = finish_stream(cs);
}

static struct stream stream_0x1590390;
static void
build_stream_0x1590390(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 1 from stream 0x1800000, 20 dwords */

	begin_stream(cs, 4);

	wr(cs, GRAS_LRZ_CNTL, 0x00000000);
	wr(cs, RB_LRZ_CNTL, 0x00000000);
	wr(cs, RB_DEPTH_PLANE_CNTL, 0x00000000);
	wr(cs, GRAS_SU_DEPTH_PLANE_CNTL, 0x00000000);
	cp(cs, CP_REG_WRITE, 0x00000002, 0x00008801, 0x00000010);
	wr(cs, SP_VS_CTRL_REG0, 0x00000200);
	wr(cs, SP_FS_CTRL_REG0, 0x81500080);
	wr(cs, SP_HS_CTRL_REG0, 0x00004200);
	wr(cs, SP_DS_CTRL_REG0, 0x00000480);

	stream_0x1590390 = finish_stream(cs);
}

static struct stream stream_0x1590120;
static void
build_stream_0x1590120(struct allocator *cs)
{
	/* VFD_FETCH0_BASE_LO from stream 0x1590180, 84 bytes */

	static const uint32_t contents[] = {
		0xbf000000, 0xbf666666, 0x00000000, 0xbfb33333, 0x3f666666, 0x00000000, 0x3ecccccc, 0x3f666666, 
		0x00000000, 0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590120 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590144;
static void
build_stream_0x1590144(struct allocator *cs)
{
	/* VFD_FETCH1_BASE_LO from stream 0x1590180, 48 bytes */

	static const uint32_t contents[] = {
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 
		0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590144 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590180;
static void
build_stream_0x1590180(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 5 from stream 0x1800000, 19 dwords */

	begin_stream(cs, 4);

	wr(cs, VFD_CONTROL_0, 0x00000202);
	wr(cs, VFD_FETCH0_BASE_LO, stream_0x1590120.offset,
	    /* VFD_FETCH0_BASE_HI */ stream_0x1590120.offset >> 32,
	    /* VFD_FETCH0_SIZE */ DIV_ROUND_UP(stream_0x1590120.size, 1),
	    /* VFD_FETCH0_STRIDE */ 0x0000000c,
	    /* VFD_FETCH1_BASE_LO */ stream_0x1590144.offset,
	    /* VFD_FETCH1_BASE_HI */ stream_0x1590144.offset >> 32,
	    /* VFD_FETCH1_SIZE */ DIV_ROUND_UP(stream_0x1590144.size, 1),
	    /* VFD_FETCH1_STRIDE */ 0x00000010);
	wr(cs, VFD_DECODE0_INSTR, 0xc7400000,
	    /* VFD_DECODE0_STEP_RATE */ 0x00000001,
	    /* VFD_DECODE1_INSTR */ 0xc8200001,
	    /* VFD_DECODE1_STEP_RATE */ 0x00000001);
	wr(cs, VFD_DEST_CNTL0_INSTR, 0x0000002f,
	    /* VFD_DEST_CNTL1_INSTR */ 0x0000006f);

	stream_0x1590180 = finish_stream(cs);
}

static struct stream stream_0x1034000;
static void
build_stream_0x1034000(struct allocator *cs)
{
	/* vertex shader instructions */

	static const uint64_t contents[] = {
		0x0300000000000000ul,	// 0000[03000000x_00000000x] end
		0x46f0000a20060000ul,
		0x4390000b203f0000ul,
		0x2024400c00000021ul,
		0x0000000000000000ul,
		0x4390000a201f000aul,
		0x4610080b1020000bul,
		0x6206000c000b1030ul,
		0x6205000b000b1030ul,
		0x6205000a000c1030ul,
		0x0000040000000000ul,
		0xc2c2170004800004ul,
		0x0000000000000000ul,
		0xc2c215000480000cul,
		0x0480000000000000ul,
		0x0500000000000000ul,

		// Register Stats:
		// - used (half): (cnt=0, max=0)
		// - used (full): (cnt=0, max=0)
		// - used (merged): (cnt=0, max=0)
		// - input (half): (cnt=0, max=0)
		// - input (full): (cnt=0, max=0)
		// - const (half): (cnt=0, max=0)
		// - const (full): (cnt=0, max=0)
		// - output (half): (cnt=0, max=0)  (estimated)
		// - output (full): (cnt=0, max=0)  (estimated)
		// - shaderdb: 1 instructions (1 instlen), 0 half, 0 full
		// - shaderdb: 0 (ss), 0 (sy)
	};

	stream_0x1034000 = allocate_block(cs, contents, sizeof(contents), 16);
}

static struct stream stream_0x1034080;
static void
build_stream_0x1034080(struct allocator *cs)
{
	/* vertex shader constants (dst_off 12) from stream 0x1035000, 64 bytes */

	static const uint32_t contents[] = {
		0x00000010, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	};

	stream_0x1034080 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1035000;
static void
build_stream_0x1035000(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 6 from stream 0x1800000, 46 dwords */

	begin_stream(cs, 4);

	wr(cs, PC_PRIMITIVE_CNTL_1, 0x00000008);
	wr(cs, VPC_UNKNOWN_9101, 0x00ffff00);
	wr(cs, GRAS_UNKNOWN_8001, 0x00000000);
	wr(cs, VPC_PACK, 0x00ff0408);
	wr(cs, VPC_GS_SIV_CNTL, 0x0000ffff);
	wr(cs, SP_PRIMITIVE_CNTL, 0x00000002);
	wr(cs, SP_VS_OUT0_REG, 0x0f020f06);
	wr(cs, SP_VS_VPC_DST0_REG, 0x00000400);
	wr(cs, SP_UNKNOWN_A81B, 0x00000000,
	    /* SP_VS_OBJ_START_LO */ stream_0x1034000.offset,
	    /* SP_VS_OBJ_START_HI */ stream_0x1034000.offset >> 32);
	wr(cs, GRAS_UNKNOWN_809B, 0x00000000);
	wr(cs, SP_VS_INSTRLEN, DIV_ROUND_UP(stream_0x1034000.size, 128));
	wr(cs, SP_VS_TEX_COUNT, 0x00000000);
	cp(cs, CP_LOAD_STATE6_GEOM, 0x00220000 | DIV_ROUND_UP(stream_0x1034000.size, 128), stream_0x1034000.offset, stream_0x1034000.offset >> 32);
	cp(cs, CP_LOAD_STATE6_GEOM, 0x01208010, 0x00000000, 0x00000000, 0x01034080, 0x00800000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000);
	cp(cs, CP_LOAD_STATE6_GEOM, 0x0022400c | DIV_ROUND_UP(stream_0x1034080.size, 16), stream_0x1034080.offset, stream_0x1034080.offset >> 32);

	stream_0x1035000 = finish_stream(cs);
}

static struct stream stream_0x1032000;
static void
build_stream_0x1032000(struct allocator *cs)
{
	/* fragment shader instructions */

	static const uint64_t contents[] = {
		0x4730cb0400002000ul,	// 0000[4730cb04x_00002000x] (rpt3)bary.f (ei)hr1.x, (r)0, r0.x
		0x0300000000000000ul,	// 0001[03000000x_00000000x] end
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,

		// Register Stats:
		// - used (half): 4-7 (cnt=4, max=7)
		// - used (full): 0 (cnt=1, max=0)
		// - used (merged): 0-1 4-7 (cnt=6, max=7)
		// - input (half): (cnt=0, max=0)
		// - input (full): 0 (cnt=1, max=0)
		// - const (half): (cnt=0, max=0)
		// - const (full): (cnt=0, max=0)
		// - output (half): 4-7 (cnt=4, max=7)  (estimated)
		// - output (full): (cnt=0, max=0)  (estimated)
		// - shaderdb: 5 instructions (2 instlen), 2 half, 0 full
		// - shaderdb: 0 (ss), 0 (sy)
	};

	stream_0x1032000 = allocate_block(cs, contents, sizeof(contents), 16);
}

static struct stream stream_0x1033000;
static void
build_stream_0x1033000(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 8 from stream 0x1800000, 49 dwords */

	begin_stream(cs, 4);

	wr(cs, VPC_VARYING_INTERP0_MODE, 0xeaeaea00);
	wr(cs, VPC_VARYING_PS_REPL0_MODE, 0x00000000);
	wr(cs, VPC_VAR0_DISABLE, 0xfffffff0,
	    /* VPC_VAR1_DISABLE */ 0xffffffff,
	    /* VPC_VAR2_DISABLE */ 0xffffffff,
	    /* VPC_VAR3_DISABLE */ 0xffffffff);
	wr(cs, VPC_CNTL_0, 0xff01ff04);
	wr(cs, SP_UNKNOWN_A982, 0x00000000,
	    /* SP_FS_OBJ_START_LO */ stream_0x1032000.offset,
	    /* SP_FS_OBJ_START_HI */ stream_0x1032000.offset >> 32);
	wr(cs, SP_FS_TEX_COUNT, 0x00000000);
	wr(cs, SP_FS_INSTRLEN, DIV_ROUND_UP(stream_0x1032000.size, 128));
	wr(cs, SP_UNKNOWN_A99E, 0x00007fc0);
	wr(cs, HLSQ_UNKNOWN_B980, 0x00000003);
	wr(cs, GRAS_UNKNOWN_8110, 0x00000002);
	wr(cs, PC_UNKNOWN_9806, 0x00000000);
	wr(cs, SP_UNKNOWN_B183, 0x00000000);
	wr(cs, HLSQ_CONTROL_2_REG, 0xfcfcfcfc,
	    /* HLSQ_CONTROL_3_REG */ 0xfcfcfc00,
	    /* HLSQ_CONTROL_4_REG */ 0xfcfcfcfc);
	wr(cs, GRAS_CNTL, 0x00000001);
	wr(cs, RB_RENDER_CONTROL0, 0x00000401);
	wr(cs, RB_RENDER_CONTROL1, 0x00000000);
	wr(cs, RB_SAMPLE_CNTL, 0x00000000);
	wr(cs, GRAS_UNKNOWN_8101, 0x00000000);
	wr(cs, GRAS_SAMPLE_CNTL, 0x00000000);
	cp(cs, CP_LOAD_STATE6_FRAG, 0x00320000 | DIV_ROUND_UP(stream_0x1032000.size, 128), stream_0x1032000.offset, stream_0x1032000.offset >> 32);

	stream_0x1033000 = finish_stream(cs);
}

static struct stream stream_0x103a000;
static void
build_stream_0x103a000(struct allocator *cs)
{
	/* hull shader instructions */

	static const uint64_t contents[] = {
		0x56f81802200b0000ul,	// 0000[56f81802x_200b0000x] (sy)(ss)(nop3)shr.b r0.z, r0.x, 11
		0x43980804201f0002ul,	// 0001[43980804x_201f0002x] (nop3)and.b r1.x, r0.z, 31
		0x429300f820030004ul,	// 0002[429300f8x_20030004x] cmps.u.ge p0.x, r1.x, 3
		0x0000050000000000ul,	// 0003[00000500x_00000000x] (rpt5)nop
		0x0080000000000023ul,	// 0004[00800000x_00000023x] br p0.x, #35
		0x43900003203f0000ul,	// 0005[43900003x_203f0000x] and.b r0.w, r0.x, 63
		0x46d0000520040004ul,	// 0006[46d00005x_20040004x] shl.b r1.y, r1.x, 4
		0x4610000620600001ul,	// 0007[46100006x_20600001x] mul.u r1.z, r0.y, 96
		0x2024400200000020ul,	// 0008[20244002x_00000020x] mov.f32f32 r0.z, c8.x
		0x4610000710250003ul,	// 0009[46100007x_10250003x] mul.u r1.w, r0.w, c9.y
		0x2024400300000021ul,	// 0010[20244003x_00000021x] mov.f32f32 r0.w, c8.y
		0x42b400f820000004ul,	// 0011[42b400f8x_20000004x] cmps.s.eq p0.x, r1.x, 0
		0x2055400400000000ul,	// 0012[20554004x_00000000x] mov.s32s32 r1.x, 0
		0x2024400800000024ul,	// 0013[20244008x_00000024x] mov.f32f32 r2.x, c9.x
		0x0000010000000000ul,	// 0014[00000100x_00000000x] (rpt1)nop
		0x4230000400050004ul,	// 0015[42300004x_00050004x] add.s r1.x, r1.x, r1.y
		0x6204000500079030ul,	// 0016[62040005x_00079030x] (nop2)mad.u24 r1.y, c12.x, r2.x, r1.w
		0x4230000800070004ul,	// 0017[42300008x_00070004x] add.s r2.x, r1.x, r1.w
		0x4230000500050004ul,	// 0018[42300005x_00050004x] add.s r1.y, r1.x, r1.y
		0x4230000700060004ul,	// 0019[42300007x_00060004x] add.s r1.w, r1.x, r1.z
		0x4238000400060004ul,	// 0020[42380004x_00060004x] (nop2)add.s r1.x, r1.x, r1.z
		0x46f0000c20020007ul,	// 0021[46f0000cx_20020007x] shr.b r3.x, r1.w, 2
		0x46f8000d20020004ul,	// 0022[46f8000dx_20020004x] (nop2)shr.b r3.y, r1.x, 2
		0xc286000404814001ul,	// 0023[c2860004x_04814001x] ldlw.u32 r1.x, l[r1.y], 4
		0xc286000804820001ul,	// 0024[c2860008x_04820001x] ldlw.u32 r2.x, l[r2.x], 4
		0x4230000c200c000cul,	// 0025[4230000cx_200c000cx] add.s r3.x, r3.x, 12
		0x0000150000000000ul,	// 0026[00001500x_00000000x] (ss)(rpt5)nop
		0xc0d2050c04800008ul,	// 0027[c0d2050cx_04800008x] stg.f32 g[r0.z+r3.x], r1.x, 4
		0xc0d2050d04800010ul,	// 0028[c0d2050dx_04800010x] stg.f32 g[r0.z+r3.y], r2.x, 4
		0x0682000000000000ul,	// 0029[06820000x_00000000x] unknown(0,13)
		0x4610100520140001ul,	// 0030[46101005x_20140001x] (ss)mul.u r1.y, r0.y, 20
		0x2024490200000022ul,	// 0031[20244902x_00000022x] (rpt1)mov.f32f32 r0.z, (r)c8.z
		0x2044400440000000ul,	// 0032[20444004x_40000000x] mov.f32f32 r1.x, (2.000000)
		0x46f0000820020005ul,	// 0033[46f00008x_20020005x] shr.b r2.x, r1.y, 2
		0x2044420540000000ul,	// 0034[20444205x_40000000x] (rpt2)mov.f32f32 r1.y, (2.000000)
		0x4230000820010008ul,	// 0035[42300008x_20010008x] add.s r2.x, r2.x, 1
		0x0000050000000000ul,	// 0036[00000500x_00000000x] (rpt5)nop
		0xc0d2050804800008ul,	// 0037[c0d20508x_04800008x] stg.f32 g[r0.z+r2.x], r1.x, 4
		0x0782000000000000ul,	// 0038[07820000x_00000000x] unknown(0,15)
		0x0800000000000000ul,	// 0039[08000000x_00000000x] (jp)nop
		0x0300000000000000ul,	// 0040[03000000x_00000000x] end
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,

		// Register Stats:
		// - used (half): (cnt=0, max=0)
		// - used (full): 0-8 12-13 (cnt=11, max=13)
		// - used (merged): 0-17 24-27 (cnt=22, max=27)
		// - input (half): (cnt=0, max=0)
		// - input (full): 0-1 (cnt=2, max=1)
		// - const (half): (cnt=0, max=0)
		// - const (full): 32-37 48 (cnt=7, max=48)
		// - output (half): (cnt=0, max=0)  (estimated)
		// - output (full): 3 5-7 (cnt=4, max=7)  (estimated)
		// - shaderdb: 60 instructions (41 instlen), 0 half, 4 full
		// - shaderdb: 3 (ss), 1 (sy)
	};

	stream_0x103a000 = allocate_block(cs, contents, sizeof(contents), 16);
}

static struct stream stream_0x103a180;
static void
build_stream_0x103a180(struct allocator *cs)
{
	/* hull shader constants (dst_off 12) from stream 0x103b000, 64 bytes */

	static const uint32_t contents[] = {
		0x00000010, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	};

	stream_0x103a180 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x103b000;
static void
build_stream_0x103b000(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 10 from stream 0x1800000, 32 dwords */

	begin_stream(cs, 4);

	wr(cs, SP_HS_UNKNOWN_A833, 0x00000000,
	    /* SP_HS_OBJ_START_LO */ stream_0x103a000.offset,
	    /* SP_HS_OBJ_START_HI */ stream_0x103a000.offset >> 32);
	wr(cs, SP_HS_TEX_COUNT, 0x00000000);
	wr(cs, SP_HS_INSTRLEN, DIV_ROUND_UP(stream_0x103a000.size, 128));
	wr(cs, PC_PRIMITIVE_CNTL_3, 0x00000000);
	wr(cs, PC_TESS_NUM_VERTEX, 0x00000003);
	cp(cs, CP_LOAD_STATE6_GEOM, 0x00260000 | DIV_ROUND_UP(stream_0x103a000.size, 128), stream_0x103a000.offset, stream_0x103a000.offset >> 32);
	cp(cs, CP_LOAD_STATE6_GEOM, 0x01248010, 0x00000000, 0x00000000, 0x0103a180, 0x00800000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000);
	cp(cs, CP_LOAD_STATE6_GEOM, 0x0026400c | DIV_ROUND_UP(stream_0x103a180.size, 16), stream_0x103a180.offset, stream_0x103a180.offset >> 32);

	stream_0x103b000 = finish_stream(cs);
}

static struct stream stream_0x1038000;
static void
build_stream_0x1038000(struct allocator *cs)
{
	/* domain shader instructions */

	static const uint64_t contents[] = {
		0x4610000520600001ul,	// 0000[46100005x_20600001x] mul.u r1.y, r0.y, 96
		0x2024400700000021ul,	// 0001[20244007x_00000021x] mov.f32f32 r1.w, c8.y
		0x2024400600000020ul,	// 0002[20244006x_00000020x] mov.f32f32 r1.z, c8.x
		0x4010000428024003ul,	// 0003[40100004x_28024003x] add.f r1.x, (neg)r0.w, 2
		0x46f8000520020005ul,	// 0004[46f80005x_20020005x] (nop2)shr.b r1.y, r1.y, 2
		0x4010000400044002ul,	// 0005[40100004x_00044002x] add.f r1.x, (neg)r0.z, r1.x
		0x42300008200c0005ul,	// 0006[42300008x_200c0005x] add.s r2.x, r1.y, 12
		0x4230000920100005ul,	// 0007[42300009x_20100005x] add.s r2.y, r1.y, 16
		0x4230000a20140005ul,	// 0008[4230000ax_20140005x] add.s r2.z, r1.y, 20
		0x4230000b20040005ul,	// 0009[4230000bx_20040005x] add.s r2.w, r1.y, 4
		0x4238000c20080005ul,	// 0010[4238000cx_20080005x] (nop2)add.s r3.x, r1.y, 8
		0xc002000d04c18011ul,	// 0011[c002000dx_04c18011x] ldg.f32 r3.y, g[r1.z+r2.x], 4
		0x0000000000000000ul,	// 0012[00000000x_00000000x] nop
		0xc002001104c18013ul,	// 0013[c0020011x_04c18013x] ldg.f32 r4.y, g[r1.z+r2.y], 4
		0x0000000000000000ul,	// 0014[00000000x_00000000x] nop
		0xc002001504c18015ul,	// 0015[c0020015x_04c18015x] ldg.f32 r5.y, g[r1.z+r2.z], 4
		0xc002001904c1800bul,	// 0016[c0020019x_04c1800bx] ldg.f32 r6.y, g[r1.z+r1.y], 4
		0x0000000000000000ul,	// 0017[00000000x_00000000x] nop
		0xc002000804c18017ul,	// 0018[c0020008x_04c18017x] ldg.f32 r2.x, g[r1.z+r2.w], 4
		0x0000000000000000ul,	// 0019[00000000x_00000000x] nop
		0xc002001d04c18019ul,	// 0020[c002001dx_04c18019x] ldg.f32 r7.y, g[r1.z+r3.x], 4
		0x5078020500110003ul,	// 0021[50780205x_00110003x] (sy)(rpt2)mul.f r1.y, r0.w, (r)r4.y
		0x4078010800080003ul,	// 0022[40780108x_00080003x] (rpt1)mul.f r2.x, r0.w, (r)r2.x
		0x4078010a000a0003ul,	// 0023[4078010ax_000a0003x] (rpt1)mul.f r2.z, r0.w, (r)r2.z
		0x4070000300140003ul,	// 0024[40700003x_00140003x] mul.f r0.w, r0.w, r5.x
		0x638c800800080002ul,	// 0025[638c8008x_00080002x] mad.f32 r2.x, r0.z, r6.y, r2.x
		0x6386800500050002ul,	// 0026[63868005x_00050002x] mad.f32 r1.y, r0.z, r3.y, r1.y
		0x638d000900090002ul,	// 0027[638d0009x_00090002x] mad.f32 r2.y, r0.z, r6.z, r2.y
		0x6387000600060002ul,	// 0028[63870006x_00060002x] mad.f32 r1.z, r0.z, r3.z, r1.z
		0x638d800a000a0002ul,	// 0029[638d800ax_000a0002x] mad.f32 r2.z, r0.z, r6.w, r2.z
		0x6387800700070002ul,	// 0030[63878007x_00070002x] mad.f32 r1.w, r0.z, r3.w, r1.w
		0x638e000b000b0002ul,	// 0031[638e000bx_000b0002x] mad.f32 r2.w, r0.z, r7.x, r2.w
		0x6388000200030002ul,	// 0032[63880002x_00030002x] mad.f32 r0.z, r0.z, r4.x, r0.w
		0x638e800800080004ul,	// 0033[638e8008x_00080004x] mad.f32 r2.x, r1.x, r7.y, r2.x
		0x638a800c00050004ul,	// 0034[638a800cx_00050004x] mad.f32 r3.x, r1.x, r5.y, r1.y
		0x638f000900090004ul,	// 0035[638f0009x_00090004x] mad.f32 r2.y, r1.x, r7.z, r2.y
		0x638b000d00060004ul,	// 0036[638b000dx_00060004x] mad.f32 r3.y, r1.x, r5.z, r1.z
		0x638f800a000a0004ul,	// 0037[638f800ax_000a0004x] mad.f32 r2.z, r1.x, r7.w, r2.z
		0x638b800e00070004ul,	// 0038[638b800ex_00070004x] mad.f32 r3.z, r1.x, r5.w, r1.w
		0x6390000b000b0004ul,	// 0039[6390000bx_000b0004x] mad.f32 r2.w, r1.x, r8.x, r2.w
		0x638c000f00020004ul,	// 0040[638c000fx_00020004x] mad.f32 r3.w, r1.x, r6.x, r0.z
		0x0300000000000000ul,	// 0041[03000000x_00000000x] end
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,
		0x0000000000000000ul,

		// Register Stats:
		// - used (half): (cnt=0, max=0)
		// - used (full): 1-32 (cnt=32, max=32)
		// - used (merged): 2-65 (cnt=64, max=65)
		// - input (half): (cnt=0, max=0)
		// - input (full): 1-3 13-32 (cnt=23, max=32)
		// - const (half): (cnt=0, max=0)
		// - const (full): 32-33 (cnt=2, max=33)
		// - output (half): (cnt=0, max=0)  (estimated)
		// - output (full): 8-15 (cnt=8, max=15)  (estimated)
		// - shaderdb: 46 instructions (42 instlen), 0 half, 8 full
		// - shaderdb: 0 (ss), 1 (sy)
	};

	stream_0x1038000 = allocate_block(cs, contents, sizeof(contents), 16);
}

static struct stream stream_0x1039000;
static void
build_stream_0x1039000(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 11 from stream 0x1800000, 32 dwords */

	begin_stream(cs, 4);

	wr(cs, SP_DS_PRIMITIVE_CNTL, 0x00000002);
	wr(cs, SP_DS_OUT0_REG, 0x0f080f0c);
	wr(cs, SP_DS_VPC_DST0_REG, 0x00000400);
	wr(cs, SP_DS_UNKNOWN_A85B, 0x00000000,
	    /* SP_DS_OBJ_START_LO */ stream_0x1038000.offset,
	    /* SP_DS_OBJ_START_HI */ stream_0x1038000.offset >> 32);
	wr(cs, SP_DS_TEX_COUNT, 0x00000000);
	wr(cs, SP_DS_INSTRLEN, DIV_ROUND_UP(stream_0x1038000.size, 128));
	wr(cs, PC_PRIMITIVE_CNTL_4, 0x00000008);
	wr(cs, 0x9103, 0x00ffff00);
	wr(cs, VPC_PACK_3, 0x00ff0408);
	wr(cs, 0x9106, 0x0000ffff);
	wr(cs, 0x809d, 0x00000000);
	wr(cs, 0x8002, 0x00000000);
	wr(cs, PC_TESS_CNTL, 0x0000000c);
	cp(cs, CP_LOAD_STATE6_GEOM, 0x002a0000 | DIV_ROUND_UP(stream_0x1038000.size, 128), stream_0x1038000.offset, stream_0x1038000.offset >> 32);

	stream_0x1039000 = finish_stream(cs);
}

static struct stream stream_0x1590000;
static void
build_stream_0x1590000(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 12 from stream 0x1800000, 9 dwords */

	begin_stream(cs, 4);

	wr(cs, SP_TP_RAS_MSAA_CNTL, 0x00000000,
	    /* SP_TP_DEST_MSAA_CNTL */ 0x00000004);
	wr(cs, RB_RAS_MSAA_CNTL, 0x00000000,
	    /* RB_DEST_MSAA_CNTL */ 0x00000004);
	wr(cs, GRAS_RAS_MSAA_CNTL, 0x00000000,
	    /* GRAS_DEST_MSAA_CNTL */ 0x00000004);

	stream_0x1590000 = finish_stream(cs);
}

static struct stream stream_0x15900c0;
static void
build_stream_0x15900c0(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 13 from stream 0x1800000, 21 dwords */

	begin_stream(cs, 4);

	wr(cs, PC_PRIMITIVE_CNTL_0, 0x00000002);
	wr(cs, VFD_CONTROL_1, 0xfcfcfcfc,
	    /* VFD_CONTROL_2 */ 0x00000001,
	    /* VFD_CONTROL_3 */ 0x030201fc,
	    /* VFD_CONTROL_4 */ 0x000000fc,
	    /* VFD_CONTROL_5 */ 0x0000fcfc,
	    /* VFD_CONTROL_6 */ 0x00000000);
	wr(cs, SP_GS_UNKNOWN_A871, 0x00000000);
	wr(cs, SP_HS_UNKNOWN_A831, 0x00000008);
	wr(cs, PC_UNKNOWN_9801, 0x00000006);
	wr(cs, PC_UNKNOWN_9B06, 0x00000000);
	wr(cs, VFD_UNKNOWN_A008, 0x00000000);
	wr(cs, PC_UNKNOWN_9B07, 0x00000000);

	stream_0x15900c0 = finish_stream(cs);
}

static struct stream stream_0x15901f0;
static void
build_stream_0x15901f0(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 14 from stream 0x1800000, 12 dwords */

	begin_stream(cs, 4);

	wr(cs, GRAS_CL_VPORT_XOFFSET_0, 0x44000000,
	    /* GRAS_CL_VPORT_XSCALE_0 */ 0x44000000,
	    /* GRAS_CL_VPORT_YOFFSET_0 */ 0x44000000,
	    /* GRAS_CL_VPORT_YSCALE_0 */ 0x44000000,
	    /* GRAS_CL_VPORT_ZOFFSET_0 */ 0x3f000000,
	    /* GRAS_CL_VPORT_ZSCALE_0 */ 0x3f000000);
	wr(cs, GRAS_SC_VIEWPORT_SCISSOR_TL_0, 0x00000000,
	    /* GRAS_SC_VIEWPORT_SCISSOR_BR_0 */ 0x03ff03ff);
	wr(cs, GRAS_CL_GUARDBAND_CLIP_ADJ, 0x0005f57d);

	stream_0x15901f0 = finish_stream(cs);
}

static struct stream stream_0x15903e0;
static void
build_stream_0x15903e0(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 15 from stream 0x1800000, 39 dwords */

	begin_stream(cs, 4);

	wr(cs, GRAS_UNKNOWN_80A0, 0x00000002);
	wr(cs, HLSQ_UNKNOWN_BB11, 0x00000000);
	wr(cs, HLSQ_CONTROL_1_REG, 0x00000007);
	wr(cs, RB_FS_OUTPUT_CNTL0, 0x00000000);
	wr(cs, RB_FS_OUTPUT_CNTL1, 0x00000001);
	wr(cs, SP_FS_OUTPUT_CNTL1, 0x00000001);
	wr(cs, SP_FS_OUTPUT_CNTL0, 0xfcfcfc00);
	wr(cs, RB_BLEND_CNTL, 0xffff0101);
	wr(cs, SP_BLEND_CNTL, 0x00000101);
	wr(cs, PC_UNKNOWN_9980, 0x00000000);
	wr(cs, VPC_UNKNOWN_9107, 0x00000000);
	wr(cs, VPC_UNKNOWN_9300, 0x00000000);
	wr(cs, SP_UNKNOWN_B182, 0x00000000);
	wr(cs, SP_FS_OUTPUT0_REG, 0x00000104,
	    /* SP_FS_OUTPUT1_REG */ 0x00000104,
	    /* SP_FS_OUTPUT2_REG */ 0x00000104,
	    /* SP_FS_OUTPUT3_REG */ 0x00000104,
	    /* SP_FS_OUTPUT4_REG */ 0x00000104,
	    /* SP_FS_OUTPUT5_REG */ 0x00000104,
	    /* SP_FS_OUTPUT6_REG */ 0x00000104,
	    /* SP_FS_OUTPUT7_REG */ 0x00000104);
	wr(cs, RB_RENDER_COMPONENTS, 0x0000000f);
	wr(cs, SP_FS_RENDER_COMPONENTS, 0x0000000f);

	stream_0x15903e0 = finish_stream(cs);
}

static struct stream stream_0x1590480;
static void
build_stream_0x1590480(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 17 from stream 0x1800000, 114 dwords */

	begin_stream(cs, 4);

	wr(cs, RB_DEPTH_BUFFER_INFO, 0x00000000,
	    /* RB_DEPTH_BUFFER_PITCH */ 0x00000000,
	    /* RB_DEPTH_BUFFER_ARRAY_PITCH */ 0x00000000,
	    /* RB_DEPTH_BUFFER_BASE_LO */ 0x00000000,
	    /* RB_DEPTH_BUFFER_BASE_HI */ 0x00000000,
	    /* RB_DEPTH_BUFFER_BASE_GMEM */ 0x00000000);
	wr(cs, GRAS_SU_DEPTH_BUFFER_INFO, 0x00000000);
	wr(cs, RB_DEPTH_FLAG_BUFFER_BASE_LO, 0x00000000,
	    /* RB_DEPTH_FLAG_BUFFER_BASE_HI */ 0x00000000,
	    /* RB_DEPTH_FLAG_BUFFER_PITCH */ 0x00000000);
	wr(cs, RB_STENCIL_INFO, 0x00000000);
	wr(cs, RB_MRT0_BUF_INFO, 0x00000030,
	    /* RB_MRT0_PITCH */ 0x00000050,
	    /* RB_MRT0_ARRAY_PITCH */ DIV_ROUND_UP(stream_0x1040000.size, 1),
	    /* RB_MRT0_BASE_LO */ stream_0x1040000.offset,
	    /* RB_MRT0_BASE_HI */ stream_0x1040000.offset >> 32,
	    /* RB_MRT0_BASE_GMEM */ 0x00000000);
	wr(cs, RB_MRT_FLAG_BUFFER0_ADDR_LO, 0x00000000,
	    /* RB_MRT_FLAG_BUFFER0_ADDR_HI */ 0x00000000,
	    /* RB_MRT_FLAG_BUFFER0_PITCH */ 0x00000000);
	wr(cs, RB_MRT1_BUF_INFO, 0x00000000,
	    /* RB_MRT1_PITCH */ 0x00000000,
	    /* RB_MRT1_ARRAY_PITCH */ 0x00000000,
	    /* RB_MRT1_BASE_LO */ 0x00000000,
	    /* RB_MRT1_BASE_HI */ 0x00000000,
	    /* RB_MRT1_BASE_GMEM */ 0x00000000);
	wr(cs, RB_MRT_FLAG_BUFFER1_ADDR_LO, 0x00000000,
	    /* RB_MRT_FLAG_BUFFER1_ADDR_HI */ 0x00000000,
	    /* RB_MRT_FLAG_BUFFER1_PITCH */ 0x00000000);
	wr(cs, RB_MRT2_BUF_INFO, 0x00000000,
	    /* RB_MRT2_PITCH */ 0x00000000,
	    /* RB_MRT2_ARRAY_PITCH */ 0x00000000,
	    /* RB_MRT2_BASE_LO */ 0x00000000,
	    /* RB_MRT2_BASE_HI */ 0x00000000,
	    /* RB_MRT2_BASE_GMEM */ 0x00000000);
	wr(cs, RB_MRT_FLAG_BUFFER2_ADDR_LO, 0x00000000,
	    /* RB_MRT_FLAG_BUFFER2_ADDR_HI */ 0x00000000,
	    /* RB_MRT_FLAG_BUFFER2_PITCH */ 0x00000000);
	wr(cs, RB_MRT3_BUF_INFO, 0x00000000,
	    /* RB_MRT3_PITCH */ 0x00000000,
	    /* RB_MRT3_ARRAY_PITCH */ 0x00000000,
	    /* RB_MRT3_BASE_LO */ 0x00000000,
	    /* RB_MRT3_BASE_HI */ 0x00000000,
	    /* RB_MRT3_BASE_GMEM */ 0x00000000);
	wr(cs, RB_MRT_FLAG_BUFFER3_ADDR_LO, 0x00000000,
	    /* RB_MRT_FLAG_BUFFER3_ADDR_HI */ 0x00000000,
	    /* RB_MRT_FLAG_BUFFER3_PITCH */ 0x00000000);
	wr(cs, RB_MRT4_BUF_INFO, 0x00000000,
	    /* RB_MRT4_PITCH */ 0x00000000,
	    /* RB_MRT4_ARRAY_PITCH */ 0x00000000,
	    /* RB_MRT4_BASE_LO */ 0x00000000,
	    /* RB_MRT4_BASE_HI */ 0x00000000,
	    /* RB_MRT4_BASE_GMEM */ 0x00000000);
	wr(cs, RB_MRT_FLAG_BUFFER4_ADDR_LO, 0x00000000,
	    /* RB_MRT_FLAG_BUFFER4_ADDR_HI */ 0x00000000,
	    /* RB_MRT_FLAG_BUFFER4_PITCH */ 0x00000000);
	wr(cs, RB_MRT5_BUF_INFO, 0x00000000,
	    /* RB_MRT5_PITCH */ 0x00000000,
	    /* RB_MRT5_ARRAY_PITCH */ 0x00000000,
	    /* RB_MRT5_BASE_LO */ 0x00000000,
	    /* RB_MRT5_BASE_HI */ 0x00000000,
	    /* RB_MRT5_BASE_GMEM */ 0x00000000);
	wr(cs, RB_MRT_FLAG_BUFFER5_ADDR_LO, 0x00000000,
	    /* RB_MRT_FLAG_BUFFER5_ADDR_HI */ 0x00000000,
	    /* RB_MRT_FLAG_BUFFER5_PITCH */ 0x00000000);
	wr(cs, RB_MRT6_BUF_INFO, 0x00000000,
	    /* RB_MRT6_PITCH */ 0x00000000,
	    /* RB_MRT6_ARRAY_PITCH */ 0x00000000,
	    /* RB_MRT6_BASE_LO */ 0x00000000,
	    /* RB_MRT6_BASE_HI */ 0x00000000,
	    /* RB_MRT6_BASE_GMEM */ 0x00000000);
	wr(cs, RB_MRT_FLAG_BUFFER6_ADDR_LO, 0x00000000,
	    /* RB_MRT_FLAG_BUFFER6_ADDR_HI */ 0x00000000,
	    /* RB_MRT_FLAG_BUFFER6_PITCH */ 0x00000000);
	wr(cs, RB_MRT7_BUF_INFO, 0x00000000,
	    /* RB_MRT7_PITCH */ 0x00000000,
	    /* RB_MRT7_ARRAY_PITCH */ 0x00000000,
	    /* RB_MRT7_BASE_LO */ 0x00000000,
	    /* RB_MRT7_BASE_HI */ 0x00000000,
	    /* RB_MRT7_BASE_GMEM */ 0x00000000);
	wr(cs, RB_MRT_FLAG_BUFFER7_ADDR_LO, 0x00000000,
	    /* RB_MRT_FLAG_BUFFER7_ADDR_HI */ 0x00000000,
	    /* RB_MRT_FLAG_BUFFER7_PITCH */ 0x00000000);
	wr(cs, SP_FS_MRT0_REG, 0x00000030,
	    /* SP_FS_MRT1_REG */ 0x00000000,
	    /* SP_FS_MRT2_REG */ 0x00000000,
	    /* SP_FS_MRT3_REG */ 0x00000000,
	    /* SP_FS_MRT4_REG */ 0x00000000,
	    /* SP_FS_MRT5_REG */ 0x00000000,
	    /* SP_FS_MRT6_REG */ 0x00000000,
	    /* SP_FS_MRT7_REG */ 0x00000000);
	wr(cs, GRAS_2D_BLIT_INFO, 0x00000030);

	stream_0x1590480 = finish_stream(cs);
}

static struct stream stream_0x1590220;
static void
build_stream_0x1590220(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 18 from stream 0x1800000, 10 dwords */

	begin_stream(cs, 4);

	wr(cs, RB_DEPTH_CNTL, 0x00000000);
	wr(cs, RB_STENCIL_CONTROL, 0x00000000);
	wr(cs, RB_STENCILREF, 0x00000000);
	wr(cs, RB_STENCILMASK, 0x00000000);
	wr(cs, RB_STENCILWRMASK, 0x00000000);

	stream_0x1590220 = finish_stream(cs);
}

static struct stream stream_0x15901d0;
static void
build_stream_0x15901d0(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 19 from stream 0x1800000, 2 dwords */

	begin_stream(cs, 4);

	wr(cs, RB_MRT0_CONTROL, 0x000007e3);

	stream_0x15901d0 = finish_stream(cs);
}

static struct stream stream_0x15901e0;
static void
build_stream_0x15901e0(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 20 from stream 0x1800000, 2 dwords */

	begin_stream(cs, 4);

	wr(cs, RB_MRT0_BLEND_CONTROL, 0x00010001);

	stream_0x15901e0 = finish_stream(cs);
}

static struct stream stream_0x1590350;
static void
build_stream_0x1590350(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 21 from stream 0x1800000, 12 dwords */

	begin_stream(cs, 4);

	wr(cs, GRAS_UNKNOWN_8000, 0x00000080);
	wr(cs, GRAS_SU_POLY_OFFSET_SCALE, 0x00000000,
	    /* GRAS_SU_POLY_OFFSET_OFFSET */ 0x00000000,
	    /* GRAS_SU_POLY_OFFSET_OFFSET_CLAMP */ 0x00000000);
	wr(cs, GRAS_SU_CNTL, 0x00000014);
	wr(cs, GRAS_UNKNOWN_8004, 0x00000000);
	wr(cs, VPC_UNKNOWN_9236, 0x00000001);

	stream_0x1590350 = finish_stream(cs);
}

static struct stream stream_0x17c0000;
static void
build_stream_0x17c0000(struct allocator *cs)
{
	/* vertex shader constants (dst_off 8) from stream 0x1590090, 64 bytes */

	static const uint32_t contents[] = {
		0x00000060, 0x00000003, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	};

	stream_0x17c0000 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1660000;
static void
build_stream_0x1660000(struct allocator *cs)
{
	/* tess param buffer, 1024 bytes */

	stream_0x1660000 = allocate_block(cs, NULL, 1024, 4);
}

static struct stream stream_0x17c0040;
static void
build_stream_0x17c0040(struct allocator *cs)
{
	/* domain shader constants (dst_off 8) from stream 0x1590090, 64 bytes */

	const uint32_t contents[] = {
		stream_0x1660000.offset, stream_0x1660000.offset >> 32,
		0, 0,
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	};

	stream_0x17c0040 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1770000;
static void
build_stream_0x1770000(struct allocator *cs)
{
	/* tess factor buffer, 1024 bytes */

	static const uint32_t contents[] = {
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	};

	stream_0x1770000 = allocate_block(cs, contents, sizeof(contents), 4);
}

static struct stream stream_0x17c0080;
static void
build_stream_0x17c0080(struct allocator *cs)
{
	/* hull shader constants (dst_off 8) from stream 0x1590090, 64 bytes */

	const uint32_t contents[] = {
		stream_0x1660000.offset, stream_0x1660000.offset >> 32,
		stream_0x1770000.offset, stream_0x1770000.offset >> 32,
		0x00000003, 0x00000060, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 
		0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	};

	stream_0x17c0080 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590090;
static void
build_stream_0x1590090(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 23 from stream 0x1800000, 12 dwords */

	begin_stream(cs, 4);

	cp(cs, CP_LOAD_STATE6_GEOM, 0x00224008 | DIV_ROUND_UP(stream_0x17c0000.size, 16), stream_0x17c0000.offset, stream_0x17c0000.offset >> 32);
	cp(cs, CP_LOAD_STATE6_GEOM, 0x002a4008 | DIV_ROUND_UP(stream_0x17c0040.size, 16), stream_0x17c0040.offset, stream_0x17c0040.offset >> 32);
	cp(cs, CP_LOAD_STATE6_GEOM, 0x00264008 | DIV_ROUND_UP(stream_0x17c0080.size, 16), stream_0x17c0080.offset, stream_0x17c0080.offset >> 32);

	stream_0x1590090 = finish_stream(cs);
}

static struct stream stream_0x1590380;
static void
build_stream_0x1590380(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 25 from stream 0x1800000, 2 dwords */

	begin_stream(cs, 4);

	wr(cs, VPC_SO_BUF_CNTL, 0x00000000);

	stream_0x1590380 = finish_stream(cs);
}

static struct stream stream_0x1800000;
static void
build_stream_0x1800000(struct allocator *cs)
{
	/* CP_INDIRECT_BUFFER from stream 0x1808000, 99 dwords */

	begin_stream(cs, 4);

	cp(cs, CP_SET_SUBDRAW_SIZE, 0x00007ffe);
	wr(cs, VPC_SO_OVERRIDE, 0x00000001);
	wr(cs, VFD_INDEX_OFFSET, 0x00000000);
	wr(cs, VFD_INSTANCE_START_OFFSET, 0x00000000);
	wr(cs, GRAS_SC_SCREEN_SCISSOR_TL_0, 0x00000000,
	    /* GRAS_SC_SCREEN_SCISSOR_BR_0 */ 0x03ff03ff);
	wr(cs, RB_ALPHA_CONTROL, 0x00000e00);
	wr(cs, RB_BLEND_RED_F32, 0x00000000,
	    /* RB_BLEND_GREEN_F32 */ 0x00000000,
	    /* RB_BLEND_BLUE_F32 */ 0x00000000,
	    /* RB_BLEND_ALPHA_F32 */ 0x00000000);
	wr(cs, RB_SRGB_CNTL, 0x00000000);
	wr(cs, SP_SRGB_CNTL, 0x00000000);
	wr(cs, RB_DITHER_CNTL, 0x00005555);
	wr(cs, RB_UNKNOWN_8818, 0x00000000);
	wr(cs, RB_UNKNOWN_8819, 0x00000000,
	    /* RB_UNKNOWN_881A */ 0x00000000,
	    /* RB_UNKNOWN_881B */ 0x00000000,
	    /* RB_UNKNOWN_881C */ 0x00000000,
	    /* RB_UNKNOWN_881D */ 0x00000000,
	    /* RB_UNKNOWN_881E */ 0x00000000);
	wr(cs, SP_UNKNOWN_A9A8, 0x00000000);
	cp(cs, CP_SET_DRAW_STATE,
	   group(0x700000, stream_0x1590030),
	   group(0x1600000, stream_0x1590390),
	   0x03720000, 0x00000000, 0x00000000,
	   0x04720000, 0x00000000, 0x00000000,
	   group(0x5700000, stream_0x1590180),
	   group(0x6610000, stream_0x1035000),
	   group(0x8610000, stream_0x1033000),
	   0x09620000, 0x00000000, 0x00000000,
	   group(0xa610000, stream_0x103b000),
	   group(0xb610000, stream_0x1039000),
	   group(0xc700000, stream_0x1590000),
	   group(0xd700000, stream_0x15900c0),
	   group(0xe700000, stream_0x15901f0),
	   group(0xf700000, stream_0x15903e0),
	   group(0x11600000, stream_0x1590480),
	   group(0x12700000, stream_0x1590220),
	   group(0x13600000, stream_0x15901d0),
	   group(0x14600000, stream_0x15901e0),
	   group(0x15700000, stream_0x1590350),
	   group(0x17700000, stream_0x1590090),
	   group(0x19700000, stream_0x1590380));

	stream_0x1800000 = finish_stream(cs);
}

static struct stream stream_0x15902d0;
static void
build_stream_0x15902d0(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 15 from stream 0x15501bc, 32 dwords */

	begin_stream(cs, 4);

	wr(cs, GRAS_UNKNOWN_80A0, 0x00000002);
	wr(cs, HLSQ_UNKNOWN_BB11, 0x00000000);
	wr(cs, HLSQ_CONTROL_1_REG, 0x00000007);
	wr(cs, RB_FS_OUTPUT_CNTL0, 0x00000000);
	wr(cs, RB_FS_OUTPUT_CNTL1, 0x00000001);
	wr(cs, SP_FS_OUTPUT_CNTL1, 0x00000001);
	wr(cs, SP_FS_OUTPUT_CNTL0, 0xfcfcfc00);
	wr(cs, RB_BLEND_CNTL, 0xffff0101);
	wr(cs, SP_BLEND_CNTL, 0x00000101);
	wr(cs, PC_UNKNOWN_9980, 0x00000000);
	wr(cs, VPC_UNKNOWN_9107, 0x00000000);
	wr(cs, VPC_UNKNOWN_9300, 0x00000000);
	wr(cs, SP_UNKNOWN_B182, 0x00000000);
	wr(cs, SP_FS_OUTPUT0_REG, 0x00000104);
	wr(cs, RB_RENDER_COMPONENTS, 0x0000000f);
	wr(cs, SP_FS_RENDER_COMPONENTS, 0x0000000f);

	stream_0x15902d0 = finish_stream(cs);
}

static struct stream stream_0x1590250;
static void
build_stream_0x1590250(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 17 from stream 0x15501bc, 30 dwords */

	begin_stream(cs, 4);

	wr(cs, RB_DEPTH_BUFFER_INFO, 0x00000000,
	    /* RB_DEPTH_BUFFER_PITCH */ 0x00000000,
	    /* RB_DEPTH_BUFFER_ARRAY_PITCH */ 0x00000000,
	    /* RB_DEPTH_BUFFER_BASE_LO */ 0x00000000,
	    /* RB_DEPTH_BUFFER_BASE_HI */ 0x00000000,
	    /* RB_DEPTH_BUFFER_BASE_GMEM */ 0x00000000);
	wr(cs, GRAS_SU_DEPTH_BUFFER_INFO, 0x00000000);
	wr(cs, RB_DEPTH_FLAG_BUFFER_BASE_LO, 0x00000000,
	    /* RB_DEPTH_FLAG_BUFFER_BASE_HI */ 0x00000000,
	    /* RB_DEPTH_FLAG_BUFFER_PITCH */ 0x00000000);
	wr(cs, RB_STENCIL_INFO, 0x00000000);
	wr(cs, RB_MRT0_BUF_INFO, 0x00000030,
	    /* RB_MRT0_PITCH */ 0x00000050,
	    /* RB_MRT0_ARRAY_PITCH */ DIV_ROUND_UP(stream_0x1040000.size, 1),
	    /* RB_MRT0_BASE_LO */ stream_0x1040000.offset,
	    /* RB_MRT0_BASE_HI */ stream_0x1040000.offset >> 32,
	    /* RB_MRT0_BASE_GMEM */ 0x00000000);
	wr(cs, RB_MRT_FLAG_BUFFER0_ADDR_LO, 0x00000000,
	    /* RB_MRT_FLAG_BUFFER0_ADDR_HI */ 0x00000000,
	    /* RB_MRT_FLAG_BUFFER0_PITCH */ 0x00000000);
	wr(cs, SP_FS_MRT0_REG, 0x00000030);
	wr(cs, GRAS_2D_BLIT_INFO, 0x00000030);

	stream_0x1590250 = finish_stream(cs);
}

static struct stream stream_0x1590650;
static void
build_stream_0x1590650(struct allocator *cs)
{
	/* VFD_FETCH0_BASE_LO from stream 0x15906b0, 84 bytes */

	static const uint32_t contents[] = {
		0xbecccccd, 0xbf666666, 0x00000000, 0xbfa66666, 0x3f666666, 0x00000000, 0x3effffff, 0x3f666666, 
		0x00000000, 0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590650 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590674;
static void
build_stream_0x1590674(struct allocator *cs)
{
	/* VFD_FETCH1_BASE_LO from stream 0x15906b0, 48 bytes */

	static const uint32_t contents[] = {
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 
		0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590674 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x15906b0;
static void
build_stream_0x15906b0(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 5 from stream 0x15501bc, 19 dwords */

	begin_stream(cs, 4);

	wr(cs, VFD_CONTROL_0, 0x00000202);
	wr(cs, VFD_FETCH0_BASE_LO, stream_0x1590650.offset,
	    /* VFD_FETCH0_BASE_HI */ stream_0x1590650.offset >> 32,
	    /* VFD_FETCH0_SIZE */ DIV_ROUND_UP(stream_0x1590650.size, 1),
	    /* VFD_FETCH0_STRIDE */ 0x0000000c,
	    /* VFD_FETCH1_BASE_LO */ stream_0x1590674.offset,
	    /* VFD_FETCH1_BASE_HI */ stream_0x1590674.offset >> 32,
	    /* VFD_FETCH1_SIZE */ DIV_ROUND_UP(stream_0x1590674.size, 1),
	    /* VFD_FETCH1_STRIDE */ 0x00000010);
	wr(cs, VFD_DECODE0_INSTR, 0xc7400000,
	    /* VFD_DECODE0_STEP_RATE */ 0x00000001,
	    /* VFD_DECODE1_INSTR */ 0xc8200001,
	    /* VFD_DECODE1_STEP_RATE */ 0x00000001);
	wr(cs, VFD_DEST_CNTL0_INSTR, 0x0000002f,
	    /* VFD_DEST_CNTL1_INSTR */ 0x0000006f);

	stream_0x15906b0 = finish_stream(cs);
}

static struct stream stream_0x1590700;
static void
build_stream_0x1590700(struct allocator *cs)
{
	/* VFD_FETCH0_BASE_LO from stream 0x1590760, 84 bytes */

	static const uint32_t contents[] = {
		0xbe99999a, 0xbf666666, 0x00000000, 0xbf99999a, 0x3f666666, 0x00000000, 0x3f199999, 0x3f666666, 
		0x00000000, 0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590700 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590724;
static void
build_stream_0x1590724(struct allocator *cs)
{
	/* VFD_FETCH1_BASE_LO from stream 0x1590760, 48 bytes */

	static const uint32_t contents[] = {
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 
		0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590724 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590760;
static void
build_stream_0x1590760(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 5 from stream 0x15501bc, 19 dwords */

	begin_stream(cs, 4);

	wr(cs, VFD_CONTROL_0, 0x00000202);
	wr(cs, VFD_FETCH0_BASE_LO, stream_0x1590700.offset,
	    /* VFD_FETCH0_BASE_HI */ stream_0x1590700.offset >> 32,
	    /* VFD_FETCH0_SIZE */ DIV_ROUND_UP(stream_0x1590700.size, 1),
	    /* VFD_FETCH0_STRIDE */ 0x0000000c,
	    /* VFD_FETCH1_BASE_LO */ stream_0x1590724.offset,
	    /* VFD_FETCH1_BASE_HI */ stream_0x1590724.offset >> 32,
	    /* VFD_FETCH1_SIZE */ DIV_ROUND_UP(stream_0x1590724.size, 1),
	    /* VFD_FETCH1_STRIDE */ 0x00000010);
	wr(cs, VFD_DECODE0_INSTR, 0xc7400000,
	    /* VFD_DECODE0_STEP_RATE */ 0x00000001,
	    /* VFD_DECODE1_INSTR */ 0xc8200001,
	    /* VFD_DECODE1_STEP_RATE */ 0x00000001);
	wr(cs, VFD_DEST_CNTL0_INSTR, 0x0000002f,
	    /* VFD_DEST_CNTL1_INSTR */ 0x0000006f);

	stream_0x1590760 = finish_stream(cs);
}

static struct stream stream_0x15907b0;
static void
build_stream_0x15907b0(struct allocator *cs)
{
	/* VFD_FETCH0_BASE_LO from stream 0x1590810, 84 bytes */

	static const uint32_t contents[] = {
		0xbe4cccce, 0xbf666666, 0x00000000, 0xbf8ccccd, 0x3f666666, 0x00000000, 0x3f333332, 0x3f666666, 
		0x00000000, 0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x15907b0 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x15907d4;
static void
build_stream_0x15907d4(struct allocator *cs)
{
	/* VFD_FETCH1_BASE_LO from stream 0x1590810, 48 bytes */

	static const uint32_t contents[] = {
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 
		0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x15907d4 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590810;
static void
build_stream_0x1590810(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 5 from stream 0x15501bc, 19 dwords */

	begin_stream(cs, 4);

	wr(cs, VFD_CONTROL_0, 0x00000202);
	wr(cs, VFD_FETCH0_BASE_LO, stream_0x15907b0.offset,
	    /* VFD_FETCH0_BASE_HI */ stream_0x15907b0.offset >> 32,
	    /* VFD_FETCH0_SIZE */ DIV_ROUND_UP(stream_0x15907b0.size, 1),
	    /* VFD_FETCH0_STRIDE */ 0x0000000c,
	    /* VFD_FETCH1_BASE_LO */ stream_0x15907d4.offset,
	    /* VFD_FETCH1_BASE_HI */ stream_0x15907d4.offset >> 32,
	    /* VFD_FETCH1_SIZE */ DIV_ROUND_UP(stream_0x15907d4.size, 1),
	    /* VFD_FETCH1_STRIDE */ 0x00000010);
	wr(cs, VFD_DECODE0_INSTR, 0xc7400000,
	    /* VFD_DECODE0_STEP_RATE */ 0x00000001,
	    /* VFD_DECODE1_INSTR */ 0xc8200001,
	    /* VFD_DECODE1_STEP_RATE */ 0x00000001);
	wr(cs, VFD_DEST_CNTL0_INSTR, 0x0000002f,
	    /* VFD_DEST_CNTL1_INSTR */ 0x0000006f);

	stream_0x1590810 = finish_stream(cs);
}

static struct stream stream_0x1590860;
static void
build_stream_0x1590860(struct allocator *cs)
{
	/* VFD_FETCH0_BASE_LO from stream 0x15908c0, 84 bytes */

	static const uint32_t contents[] = {
		0xbdcccccf, 0xbf666666, 0x00000000, 0xbf800000, 0x3f666666, 0x00000000, 0x3f4ccccc, 0x3f666666, 
		0x00000000, 0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590860 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590884;
static void
build_stream_0x1590884(struct allocator *cs)
{
	/* VFD_FETCH1_BASE_LO from stream 0x15908c0, 48 bytes */

	static const uint32_t contents[] = {
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 
		0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590884 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x15908c0;
static void
build_stream_0x15908c0(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 5 from stream 0x15501bc, 19 dwords */

	begin_stream(cs, 4);

	wr(cs, VFD_CONTROL_0, 0x00000202);
	wr(cs, VFD_FETCH0_BASE_LO, stream_0x1590860.offset,
	    /* VFD_FETCH0_BASE_HI */ stream_0x1590860.offset >> 32,
	    /* VFD_FETCH0_SIZE */ DIV_ROUND_UP(stream_0x1590860.size, 1),
	    /* VFD_FETCH0_STRIDE */ 0x0000000c,
	    /* VFD_FETCH1_BASE_LO */ stream_0x1590884.offset,
	    /* VFD_FETCH1_BASE_HI */ stream_0x1590884.offset >> 32,
	    /* VFD_FETCH1_SIZE */ DIV_ROUND_UP(stream_0x1590884.size, 1),
	    /* VFD_FETCH1_STRIDE */ 0x00000010);
	wr(cs, VFD_DECODE0_INSTR, 0xc7400000,
	    /* VFD_DECODE0_STEP_RATE */ 0x00000001,
	    /* VFD_DECODE1_INSTR */ 0xc8200001,
	    /* VFD_DECODE1_STEP_RATE */ 0x00000001);
	wr(cs, VFD_DEST_CNTL0_INSTR, 0x0000002f,
	    /* VFD_DEST_CNTL1_INSTR */ 0x0000006f);

	stream_0x15908c0 = finish_stream(cs);
}

static struct stream stream_0x1590910;
static void
build_stream_0x1590910(struct allocator *cs)
{
	/* VFD_FETCH0_BASE_LO from stream 0x1590970, 84 bytes */

	static const uint32_t contents[] = {
		0xb2800000, 0xbf666666, 0x00000000, 0xbf666666, 0x3f666666, 0x00000000, 0x3f666666, 0x3f666666, 
		0x00000000, 0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590910 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590934;
static void
build_stream_0x1590934(struct allocator *cs)
{
	/* VFD_FETCH1_BASE_LO from stream 0x1590970, 48 bytes */

	static const uint32_t contents[] = {
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 
		0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590934 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590970;
static void
build_stream_0x1590970(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 5 from stream 0x15501bc, 19 dwords */

	begin_stream(cs, 4);

	wr(cs, VFD_CONTROL_0, 0x00000202);
	wr(cs, VFD_FETCH0_BASE_LO, stream_0x1590910.offset,
	    /* VFD_FETCH0_BASE_HI */ stream_0x1590910.offset >> 32,
	    /* VFD_FETCH0_SIZE */ DIV_ROUND_UP(stream_0x1590910.size, 1),
	    /* VFD_FETCH0_STRIDE */ 0x0000000c,
	    /* VFD_FETCH1_BASE_LO */ stream_0x1590934.offset,
	    /* VFD_FETCH1_BASE_HI */ stream_0x1590934.offset >> 32,
	    /* VFD_FETCH1_SIZE */ DIV_ROUND_UP(stream_0x1590934.size, 1),
	    /* VFD_FETCH1_STRIDE */ 0x00000010);
	wr(cs, VFD_DECODE0_INSTR, 0xc7400000,
	    /* VFD_DECODE0_STEP_RATE */ 0x00000001,
	    /* VFD_DECODE1_INSTR */ 0xc8200001,
	    /* VFD_DECODE1_STEP_RATE */ 0x00000001);
	wr(cs, VFD_DEST_CNTL0_INSTR, 0x0000002f,
	    /* VFD_DEST_CNTL1_INSTR */ 0x0000006f);

	stream_0x1590970 = finish_stream(cs);
}

static struct stream stream_0x15909c0;
static void
build_stream_0x15909c0(struct allocator *cs)
{
	/* VFD_FETCH0_BASE_LO from stream 0x1590a20, 84 bytes */

	static const uint32_t contents[] = {
		0x3dcccccb, 0xbf666666, 0x00000000, 0xbf4ccccd, 0x3f666666, 0x00000000, 0x3f7fffff, 0x3f666666, 
		0x00000000, 0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x15909c0 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x15909e4;
static void
build_stream_0x15909e4(struct allocator *cs)
{
	/* VFD_FETCH1_BASE_LO from stream 0x1590a20, 48 bytes */

	static const uint32_t contents[] = {
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 
		0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x15909e4 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590a20;
static void
build_stream_0x1590a20(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 5 from stream 0x15501bc, 19 dwords */

	begin_stream(cs, 4);

	wr(cs, VFD_CONTROL_0, 0x00000202);
	wr(cs, VFD_FETCH0_BASE_LO, stream_0x15909c0.offset,
	    /* VFD_FETCH0_BASE_HI */ stream_0x15909c0.offset >> 32,
	    /* VFD_FETCH0_SIZE */ DIV_ROUND_UP(stream_0x15909c0.size, 1),
	    /* VFD_FETCH0_STRIDE */ 0x0000000c,
	    /* VFD_FETCH1_BASE_LO */ stream_0x15909e4.offset,
	    /* VFD_FETCH1_BASE_HI */ stream_0x15909e4.offset >> 32,
	    /* VFD_FETCH1_SIZE */ DIV_ROUND_UP(stream_0x15909e4.size, 1),
	    /* VFD_FETCH1_STRIDE */ 0x00000010);
	wr(cs, VFD_DECODE0_INSTR, 0xc7400000,
	    /* VFD_DECODE0_STEP_RATE */ 0x00000001,
	    /* VFD_DECODE1_INSTR */ 0xc8200001,
	    /* VFD_DECODE1_STEP_RATE */ 0x00000001);
	wr(cs, VFD_DEST_CNTL0_INSTR, 0x0000002f,
	    /* VFD_DEST_CNTL1_INSTR */ 0x0000006f);

	stream_0x1590a20 = finish_stream(cs);
}

static struct stream stream_0x1590a70;
static void
build_stream_0x1590a70(struct allocator *cs)
{
	/* VFD_FETCH0_BASE_LO from stream 0x1590ad0, 84 bytes */

	static const uint32_t contents[] = {
		0x3e4ccccc, 0xbf666666, 0x00000000, 0xbf333333, 0x3f666666, 0x00000000, 0x3f8ccccc, 0x3f666666, 
		0x00000000, 0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590a70 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590a94;
static void
build_stream_0x1590a94(struct allocator *cs)
{
	/* VFD_FETCH1_BASE_LO from stream 0x1590ad0, 48 bytes */

	static const uint32_t contents[] = {
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 
		0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590a94 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590ad0;
static void
build_stream_0x1590ad0(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 5 from stream 0x15501bc, 19 dwords */

	begin_stream(cs, 4);

	wr(cs, VFD_CONTROL_0, 0x00000202);
	wr(cs, VFD_FETCH0_BASE_LO, stream_0x1590a70.offset,
	    /* VFD_FETCH0_BASE_HI */ stream_0x1590a70.offset >> 32,
	    /* VFD_FETCH0_SIZE */ DIV_ROUND_UP(stream_0x1590a70.size, 1),
	    /* VFD_FETCH0_STRIDE */ 0x0000000c,
	    /* VFD_FETCH1_BASE_LO */ stream_0x1590a94.offset,
	    /* VFD_FETCH1_BASE_HI */ stream_0x1590a94.offset >> 32,
	    /* VFD_FETCH1_SIZE */ DIV_ROUND_UP(stream_0x1590a94.size, 1),
	    /* VFD_FETCH1_STRIDE */ 0x00000010);
	wr(cs, VFD_DECODE0_INSTR, 0xc7400000,
	    /* VFD_DECODE0_STEP_RATE */ 0x00000001,
	    /* VFD_DECODE1_INSTR */ 0xc8200001,
	    /* VFD_DECODE1_STEP_RATE */ 0x00000001);
	wr(cs, VFD_DEST_CNTL0_INSTR, 0x0000002f,
	    /* VFD_DEST_CNTL1_INSTR */ 0x0000006f);

	stream_0x1590ad0 = finish_stream(cs);
}

static struct stream stream_0x1590b20;
static void
build_stream_0x1590b20(struct allocator *cs)
{
	/* VFD_FETCH0_BASE_LO from stream 0x1590b80, 84 bytes */

	static const uint32_t contents[] = {
		0x3e999999, 0xbf666666, 0x00000000, 0xbf19999a, 0x3f666666, 0x00000000, 0x3f999999, 0x3f666666, 
		0x00000000, 0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590b20 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590b44;
static void
build_stream_0x1590b44(struct allocator *cs)
{
	/* VFD_FETCH1_BASE_LO from stream 0x1590b80, 48 bytes */

	static const uint32_t contents[] = {
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 
		0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590b44 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590b80;
static void
build_stream_0x1590b80(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 5 from stream 0x15501bc, 19 dwords */

	begin_stream(cs, 4);

	wr(cs, VFD_CONTROL_0, 0x00000202);
	wr(cs, VFD_FETCH0_BASE_LO, stream_0x1590b20.offset,
	    /* VFD_FETCH0_BASE_HI */ stream_0x1590b20.offset >> 32,
	    /* VFD_FETCH0_SIZE */ DIV_ROUND_UP(stream_0x1590b20.size, 1),
	    /* VFD_FETCH0_STRIDE */ 0x0000000c,
	    /* VFD_FETCH1_BASE_LO */ stream_0x1590b44.offset,
	    /* VFD_FETCH1_BASE_HI */ stream_0x1590b44.offset >> 32,
	    /* VFD_FETCH1_SIZE */ DIV_ROUND_UP(stream_0x1590b44.size, 1),
	    /* VFD_FETCH1_STRIDE */ 0x00000010);
	wr(cs, VFD_DECODE0_INSTR, 0xc7400000,
	    /* VFD_DECODE0_STEP_RATE */ 0x00000001,
	    /* VFD_DECODE1_INSTR */ 0xc8200001,
	    /* VFD_DECODE1_STEP_RATE */ 0x00000001);
	wr(cs, VFD_DEST_CNTL0_INSTR, 0x0000002f,
	    /* VFD_DEST_CNTL1_INSTR */ 0x0000006f);

	stream_0x1590b80 = finish_stream(cs);
}

static struct stream stream_0x1590bd0;
static void
build_stream_0x1590bd0(struct allocator *cs)
{
	/* VFD_FETCH0_BASE_LO from stream 0x1590c30, 84 bytes */

	static const uint32_t contents[] = {
		0x3ecccccc, 0xbf666666, 0x00000000, 0xbf000000, 0x3f666666, 0x00000000, 0x3fa66666, 0x3f666666, 
		0x00000000, 0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590bd0 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590bf4;
static void
build_stream_0x1590bf4(struct allocator *cs)
{
	/* VFD_FETCH1_BASE_LO from stream 0x1590c30, 48 bytes */

	static const uint32_t contents[] = {
		0x3f800000, 0x00000000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 0x00000000, 0x3f800000, 
		0x00000000, 0x00000000, 0x3f800000, 0x00000000, 
	};

	stream_0x1590bf4 = allocate_block(cs, contents, sizeof(contents), 64);
}

static struct stream stream_0x1590c30;
static void
build_stream_0x1590c30(struct allocator *cs)
{
	/* CP_SET_DRAW_STATE group 5 from stream 0x15501bc, 19 dwords */

	begin_stream(cs, 4);

	wr(cs, VFD_CONTROL_0, 0x00000202);
	wr(cs, VFD_FETCH0_BASE_LO, stream_0x1590bd0.offset,
	    /* VFD_FETCH0_BASE_HI */ stream_0x1590bd0.offset >> 32,
	    /* VFD_FETCH0_SIZE */ DIV_ROUND_UP(stream_0x1590bd0.size, 1),
	    /* VFD_FETCH0_STRIDE */ 0x0000000c,
	    /* VFD_FETCH1_BASE_LO */ stream_0x1590bf4.offset,
	    /* VFD_FETCH1_BASE_HI */ stream_0x1590bf4.offset >> 32,
	    /* VFD_FETCH1_SIZE */ DIV_ROUND_UP(stream_0x1590bf4.size, 1),
	    /* VFD_FETCH1_STRIDE */ 0x00000010);
	wr(cs, VFD_DECODE0_INSTR, 0xc7400000,
	    /* VFD_DECODE0_STEP_RATE */ 0x00000001,
	    /* VFD_DECODE1_INSTR */ 0xc8200001,
	    /* VFD_DECODE1_STEP_RATE */ 0x00000001);
	wr(cs, VFD_DEST_CNTL0_INSTR, 0x0000002f,
	    /* VFD_DEST_CNTL1_INSTR */ 0x0000006f);

	stream_0x1590c30 = finish_stream(cs);
}

static struct stream stream_0x15501bc;
static void
build_stream_0x15501bc(struct allocator *cs)
{
	/* CP_INDIRECT_BUFFER from stream 0x1808000, 170 dwords */

	begin_stream(cs, 4);

	//cp(cs, CP_SET_SUBDRAW_SIZE, 0x00007ffe);
	cp(cs, CP_SET_SUBDRAW_SIZE, 0x00009ffe);
	wr(cs, RB_ALPHA_CONTROL, 0x00000e00);
	wr(cs, RB_SRGB_CNTL, 0x00000000);
	wr(cs, SP_SRGB_CNTL, 0x00000000);
	wr(cs, RB_DITHER_CNTL, 0x00005555);
	wr(cs, GRAS_SC_SCREEN_SCISSOR_TL_0, 0x00000000,
	    /* GRAS_SC_SCREEN_SCISSOR_BR_0 */ 0x03ff03ff);
	wr(cs, RB_UNKNOWN_8818, 0x00000000);
	wr(cs, RB_UNKNOWN_8819, 0x00000000,
	    /* RB_UNKNOWN_881A */ 0x00000000,
	    /* RB_UNKNOWN_881B */ 0x00000000,
	    /* RB_UNKNOWN_881C */ 0x00000000,
	    /* RB_UNKNOWN_881D */ 0x00000000,
	    /* RB_UNKNOWN_881E */ 0x00000000);
	wr(cs, SP_UNKNOWN_A9A8, 0x00000000);
	cp(cs, CP_SKIP_IB2_ENABLE_LOCAL, 0x00000001);
	wr(cs, VFD_INDEX_OFFSET, 0x00000000);
	wr(cs, VFD_INSTANCE_START_OFFSET, 0x00000000);
	cp(cs, CP_SET_DRAW_STATE,
	   group(0x700000, stream_0x1590030),
	   group(0x1600000, stream_0x1590390),
	   0x03720000, 0x00000000, 0x00000000,
	   0x04720000, 0x00000000, 0x00000000,
	   group(0x5700000, stream_0x1590180),
	   group(0x6610000, stream_0x1035000),
	   group(0x8610000, stream_0x1033000),
	   0x09620000, 0x00000000, 0x00000000,
	   group(0xa610000, stream_0x103b000),
	   group(0xb610000, stream_0x1039000),
	   group(0xc700000, stream_0x1590000),
	   group(0xd700000, stream_0x15900c0),
	   group(0xe700000, stream_0x15901f0),
	   group(0xf700000, stream_0x15902d0),
	   group(0x11600000, stream_0x1590250),
	   group(0x12700000, stream_0x1590220),
	   group(0x13600000, stream_0x15901d0),
	   group(0x14600000, stream_0x15901e0),
	   group(0x15700000, stream_0x1590350),
	   group(0x17700000, stream_0x1590090),
	   group(0x19700000, stream_0x1590380));
	cp(cs, CP_DRAW_INDX_OFFSET, 0x219a2, 0x1, 0x3);
	cp(cs, CP_SET_DRAW_STATE,
	   group(0x5700000, stream_0x15906b0));
	cp(cs, CP_DRAW_INDX_OFFSET, 0x219a2, 0x1, 0x3);
	cp(cs, CP_SET_DRAW_STATE,
	   group(0x5700000, stream_0x1590760));
	cp(cs, CP_DRAW_INDX_OFFSET, 0x219a2, 0x1, 0x3);
	cp(cs, CP_SET_DRAW_STATE,
	   group(0x5700000, stream_0x1590810));
	cp(cs, CP_DRAW_INDX_OFFSET, 0x219a2, 0x1, 0x3);
	cp(cs, CP_SET_DRAW_STATE,
	   group(0x5700000, stream_0x15908c0));
	cp(cs, CP_DRAW_INDX_OFFSET, 0x219a2, 0x1, 0x3);
	cp(cs, CP_SET_DRAW_STATE,
	   group(0x5700000, stream_0x1590970));
	cp(cs, CP_DRAW_INDX_OFFSET, 0x219a2, 0x1, 0x3);
	cp(cs, CP_SET_DRAW_STATE,
	   group(0x5700000, stream_0x1590a20));
	cp(cs, CP_DRAW_INDX_OFFSET, 0x219a2, 0x1, 0x3);
	cp(cs, CP_SET_DRAW_STATE,
	   group(0x5700000, stream_0x1590ad0));
	cp(cs, CP_DRAW_INDX_OFFSET, 0x219a2, 0x1, 0x3);
	cp(cs, CP_SET_DRAW_STATE,
	   group(0x5700000, stream_0x1590b80));
	cp(cs, CP_DRAW_INDX_OFFSET, 0x219a2, 0x1, 0x3);
	cp(cs, CP_SET_DRAW_STATE,
	   group(0x5700000, stream_0x1590c30));
	cp(cs, CP_DRAW_INDX_OFFSET, 0x219a2, 0x1, 0x3);

	stream_0x15501bc = finish_stream(cs);
}

static struct stream stream_0x1808000;
static void
build_stream_0x1808000(struct allocator *cs)
{
	/* toplevel invocation 1, 198 dwords */

	begin_stream(cs, 4);

	cp(cs, CP_WAIT_FOR_IDLE);
	wr(cs, RB_CCU_CNTL, 0x10000000);
	wr(cs, RB_UNKNOWN_8E04, 0x00100000);
	wr(cs, SP_UNKNOWN_AE04, 0x00000008);
	wr(cs, SP_UNKNOWN_AE00, 0x00000000);
	wr(cs, SP_UNKNOWN_AE0F, 0x0000003f);
	wr(cs, SP_UNKNOWN_B605, 0x00000044);
	wr(cs, SP_UNKNOWN_B600, 0x00100000);
	wr(cs, HLSQ_UNKNOWN_BE00, 0x00000080,
	    /* HLSQ_UNKNOWN_BE01 */ 0x00000000);
	wr(cs, VPC_UNKNOWN_9600, 0x00000000);
	wr(cs, GRAS_UNKNOWN_8600, 0x00000880);
	wr(cs, HLSQ_UNKNOWN_BE04, 0x00080000);
	wr(cs, SP_UNKNOWN_AE03, 0x00001430);
	wr(cs, UCHE_UNKNOWN_0E12, 0x03200000);
	wr(cs, RB_UNKNOWN_8E01, 0x00000001);
	wr(cs, SP_UNKNOWN_AB00, 0x00000005);
	wr(cs, VFD_UNKNOWN_A009, 0x00000001);
	wr(cs, RB_UNKNOWN_8811, 0x00000010);
	wr(cs, PC_MODE_CNTL, 0x0000001f);
	wr(cs, PC_RESTART_INDEX, 0xffffffff);
	wr(cs, GRAS_SU_POINT_MINMAX, 0x3ff00010,
	    /* GRAS_SU_POINT_SIZE */ 0x00000008);
	wr(cs, GRAS_UNKNOWN_8099, 0x00000000);
	wr(cs, GRAS_UNKNOWN_80AF, 0x00000000);
	wr(cs, VPC_UNKNOWN_9210, 0x00000000,
	    /* VPC_UNKNOWN_9211 */ 0x00000000);
	wr(cs, VPC_UNKNOWN_9602, 0x00000000);
	wr(cs, PC_UNKNOWN_9981, 0x00000003);
	wr(cs, PC_UNKNOWN_9E72, 0x00000000);
	wr(cs, VPC_UNKNOWN_9108, 0x00000003);
	wr(cs, SP_TP_UNKNOWN_B309, 0x000000b2);
	wr(cs, GRAS_UNKNOWN_80A5, 0x00000000);
	wr(cs, GRAS_UNKNOWN_80A6, 0x00000000);
	wr(cs, RB_UNKNOWN_8805, 0x00000000);
	wr(cs, RB_UNKNOWN_8806, 0x00000000);
	wr(cs, RB_UNKNOWN_8878, 0x00000000,
	    /* RB_UNKNOWN_8879 */ 0x00000000);
	wr(cs, HLSQ_CONTROL_5_REG, 0x000000fc);
	wr(cs, UCHE_CLIENT_PF, 0x00000004);
	wr(cs, GRAS_LRZ_CNTL, 0x00000000);
	cp(cs, CP_EVENT_WRITE, 0x26);
	cp(cs, CP_EVENT_WRITE, 0x1d, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0xb);
	cp(cs, CP_EVENT_WRITE, 0x1c, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0xc);
	cp(cs, CP_EVENT_WRITE, 0x19);
	cp(cs, CP_EVENT_WRITE, 0x18);
	cp(cs, CP_SKIP_IB2_ENABLE_GLOBAL, 0x00000000);
	cp(cs, CP_WAIT_FOR_IDLE);
	wr(cs, PC_TESSFACTOR_ADDR_LO, 0x01770000,
	    /* PC_TESSFACTOR_ADDR_HI */ 0x00000000);
	wr(cs, RB_UNKNOWN_88F0, 0x00000000);
	wr(cs, RB_BIN_CONTROL, 0x00c00000);
	wr(cs, GRAS_BIN_CONTROL, 0x00c00000);
	wr(cs, VFD_MODE_CNTL, 0x00000000);
	wr(cs, PC_UNKNOWN_9805, 0x00000001);
	wr(cs, SP_UNKNOWN_A0F8, 0x00000001);
	cp(cs, CP_SKIP_IB2_ENABLE_GLOBAL, 0x00000000);
	cp(cs, CP_SET_MARKER, 0x00000001);
	wr(cs, GRAS_SC_WINDOW_SCISSOR_TL, 0x00000000,
	    /* GRAS_SC_WINDOW_SCISSOR_BR */ 0x03ff03ff);
	wr(cs, GRAS_RESOLVE_CNTL_1, 0x00000000,
	    /* GRAS_RESOLVE_CNTL_2 */ 0x03ff03ff);
	wr(cs, RB_WINDOW_OFFSET, 0x00000000);
	wr(cs, SP_TP_WINDOW_OFFSET, 0x00000000);
	wr(cs, SP_WINDOW_OFFSET, 0x00000000);
	wr(cs, RB_WINDOW_OFFSET2, 0x00000000);
	wr(cs, RB_BIN_CONTROL2, 0x00000000);
	wr(cs, RB_BIN_CONTROL, 0x00c00000);
	wr(cs, GRAS_BIN_CONTROL, 0x00c00000);
	//ib(cs, stream_0x1800000);
	wr(cs, VPC_SO_OVERRIDE, 0x00000001);
	cp(cs, CP_SET_VISIBILITY_OVERRIDE, 0x00000001);
	cp(cs, CP_SET_MODE, 0x00000000);
	wr(cs, RB_UNKNOWN_8804, 0x00000000);
	wr(cs, SP_TP_UNKNOWN_B304, 0x00000000);
	wr(cs, GRAS_UNKNOWN_80A4, 0x00000000);
	ib(cs, stream_0x15501bc);
	cp(cs, CP_SKIP_IB2_ENABLE_LOCAL, 0x00000000);
	cp(cs, CP_SKIP_IB2_ENABLE_GLOBAL, 0x00000000);
	wr(cs, GRAS_LRZ_CNTL, 0x00000009);
	cp(cs, CP_EVENT_WRITE, 0x26);
	cp(cs, CP_EVENT_WRITE, 0x1d, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0xd);
	cp(cs, CP_EVENT_WRITE, 0x4, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0xe);
	cp(cs, CP_EVENT_WRITE, 0x31);
	cp(cs, CP_UNK_A6XX_14, 0x0, stream_0x1024000.offset, stream_0x1024000.offset >> 32, 0xe);
	wr(cs, RB_UNKNOWN_88F0, 0x00000000);

	stream_0x1808000 = finish_stream(cs);
}

static struct stream stream_0x180c000;
static void
build_stream_0x180c000(struct allocator *cs)
{
	/* toplevel invocation 2, 83 dwords */

	begin_stream(cs, 4);

	cp(cs, CP_WAIT_FOR_IDLE);
	wr(cs, RB_CCU_CNTL, 0x10000000);
	wr(cs, RB_UNKNOWN_8E04, 0x00100000);
	wr(cs, SP_UNKNOWN_AE04, 0x00000008);
	wr(cs, SP_UNKNOWN_AE00, 0x00000000);
	wr(cs, SP_UNKNOWN_AE0F, 0x0000003f);
	wr(cs, SP_UNKNOWN_B605, 0x00000044);
	wr(cs, SP_UNKNOWN_B600, 0x00100000);
	wr(cs, HLSQ_UNKNOWN_BE00, 0x00000080,
	    /* HLSQ_UNKNOWN_BE01 */ 0x00000000);
	wr(cs, VPC_UNKNOWN_9600, 0x00000000);
	wr(cs, GRAS_UNKNOWN_8600, 0x00000880);
	wr(cs, HLSQ_UNKNOWN_BE04, 0x00080000);
	wr(cs, SP_UNKNOWN_AE03, 0x00001430);
	wr(cs, UCHE_UNKNOWN_0E12, 0x03200000);
	wr(cs, RB_UNKNOWN_8E01, 0x00000001);
	wr(cs, SP_UNKNOWN_AB00, 0x00000005);
	wr(cs, VFD_UNKNOWN_A009, 0x00000001);
	wr(cs, RB_UNKNOWN_8811, 0x00000010);
	wr(cs, PC_MODE_CNTL, 0x0000001f);
	wr(cs, PC_RESTART_INDEX, 0xffffffff);
	wr(cs, GRAS_SU_POINT_MINMAX, 0x3ff00010,
	    /* GRAS_SU_POINT_SIZE */ 0x00000008);
	wr(cs, GRAS_UNKNOWN_8099, 0x00000000);
	wr(cs, GRAS_UNKNOWN_80AF, 0x00000000);
	wr(cs, VPC_UNKNOWN_9210, 0x00000000,
	    /* VPC_UNKNOWN_9211 */ 0x00000000);
	wr(cs, VPC_UNKNOWN_9602, 0x00000000);
	wr(cs, PC_UNKNOWN_9981, 0x00000003);
	wr(cs, PC_UNKNOWN_9E72, 0x00000000);
	wr(cs, VPC_UNKNOWN_9108, 0x00000003);
	wr(cs, SP_TP_UNKNOWN_B309, 0x000000b2);
	wr(cs, GRAS_UNKNOWN_80A5, 0x00000000);
	wr(cs, GRAS_UNKNOWN_80A6, 0x00000000);
	wr(cs, RB_UNKNOWN_8805, 0x00000000);
	wr(cs, RB_UNKNOWN_8806, 0x00000000);
	wr(cs, RB_UNKNOWN_8878, 0x00000000,
	    /* RB_UNKNOWN_8879 */ 0x00000000);
	wr(cs, HLSQ_CONTROL_5_REG, 0x000000fc);
	cp(cs, CP_MEM_TO_REG, 0xc38 | ((stream_0x1026000.size / 4) << 19),
	   stream_0x1026000.offset, stream_0x1026000.offset >> 32);
	wr(cs, UCHE_CLIENT_PF, 0x00000004);
	cp(cs, CP_SET_DRAW_STATE,
	   0x00040000, 0x00000000, 0x00000000);

	stream_0x180c000 = finish_stream(cs);
}

static struct stream stream_0x1020000;
static void
build_stream_0x1020000(struct allocator *cs)
{
	/* toplevel invocation 3, 2 dwords */

	begin_stream(cs, 4);

	cp(cs, CP_NOP, 0x00000000);

	stream_0x1020000 = finish_stream(cs);
}

int main(int argc, char *argv[])
{
	struct allocator cs;
	init_allocator(&cs, 64 << 20);

	build_stream_0x1025000(&cs);
	build_stream_0x1025008(&cs);
	build_stream_0x1026000(&cs);
	build_stream_0x1027000(&cs);
	build_stream_0x1024000(&cs);
	build_stream_0x1040000(&cs);
	build_stream_0x1550040(&cs);
	build_stream_0x1804000(&cs);
	build_stream_0x1590030(&cs);
	build_stream_0x1590390(&cs);
	build_stream_0x1590120(&cs);
	build_stream_0x1590144(&cs);
	build_stream_0x1590180(&cs);
	build_stream_0x1034000(&cs);
	build_stream_0x1034080(&cs);
	build_stream_0x1035000(&cs);
	build_stream_0x1032000(&cs);
	build_stream_0x1033000(&cs);
	build_stream_0x103a000(&cs);
	build_stream_0x103a180(&cs);
	build_stream_0x103b000(&cs);
	build_stream_0x1038000(&cs);
	build_stream_0x1039000(&cs);
	build_stream_0x1590000(&cs);
	build_stream_0x15900c0(&cs);
	build_stream_0x15901f0(&cs);
	build_stream_0x15903e0(&cs);
	build_stream_0x1590480(&cs);
	build_stream_0x1590220(&cs);
	build_stream_0x15901d0(&cs);
	build_stream_0x15901e0(&cs);
	build_stream_0x1590350(&cs);
	build_stream_0x17c0000(&cs);
	build_stream_0x17c0040(&cs);
	build_stream_0x1660000(&cs);
	build_stream_0x17c0080(&cs);
	build_stream_0x1770000(&cs);
	build_stream_0x1590090(&cs);
	build_stream_0x1590380(&cs);
	build_stream_0x1800000(&cs);
	build_stream_0x15902d0(&cs);
	build_stream_0x1590250(&cs);
	build_stream_0x1590650(&cs);
	build_stream_0x1590674(&cs);
	build_stream_0x15906b0(&cs);
	build_stream_0x1590700(&cs);
	build_stream_0x1590724(&cs);
	build_stream_0x1590760(&cs);
	build_stream_0x15907b0(&cs);
	build_stream_0x15907d4(&cs);
	build_stream_0x1590810(&cs);
	build_stream_0x1590860(&cs);
	build_stream_0x1590884(&cs);
	build_stream_0x15908c0(&cs);
	build_stream_0x1590910(&cs);
	build_stream_0x1590934(&cs);
	build_stream_0x1590970(&cs);
	build_stream_0x15909c0(&cs);
	build_stream_0x15909e4(&cs);
	build_stream_0x1590a20(&cs);
	build_stream_0x1590a70(&cs);
	build_stream_0x1590a94(&cs);
	build_stream_0x1590ad0(&cs);
	build_stream_0x1590b20(&cs);
	build_stream_0x1590b44(&cs);
	build_stream_0x1590b80(&cs);
	build_stream_0x1590bd0(&cs);
	build_stream_0x1590bf4(&cs);
	build_stream_0x1590c30(&cs);
	build_stream_0x15501bc(&cs);
	build_stream_0x1808000(&cs);
	build_stream_0x180c000(&cs);
	build_stream_0x1020000(&cs);

	// submit_stream(&cs, stream_0x1804000);

	submit_stream(&cs, stream_0x1808000);

	// submit_stream(&cs, stream_0x180c000);

	// submit_stream(&cs, stream_0x1020000);

	png_image pi = {
		.version = PNG_IMAGE_VERSION,
		.width = 1024,
		.height = 1024,
		.format = PNG_FORMAT_RGBA
	};

	FILE *f = fopen("tessellator.png", "wb");
	fail_if(f == NULL, "failed to open output file");

	void *buffer = cs.data + stream_0x1040000.offset;
	uint32_t stride = 5120;
	fail_if(!png_image_write_to_stdio(&pi, f, 0, buffer, stride, NULL),
		"failed to write output");

	fclose(f);


	return 0;
}
